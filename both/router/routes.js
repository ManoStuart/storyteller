/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound',
  templateNameConverter: 'upperCamelCase',
  routeControllerNameConverter: 'upperCamelCase'
});

Router.route('/', {name: 'home'});

Router.route('/chapter/new', {name: 'newChapter'});
Router.route('/chapter/edit/:id', {name: 'editChapter'});

Router.route('/run/history/:id', {name: 'runHistory'});
Router.route('/run/options/:id', {name: 'runOptions'});
Router.route('/run/share/:id', {name: 'runShare'});
Router.route('/run/:id', {name: 'run'});

Router.route('/world/edit/:id', {name: 'editWorld'});
Router.route('/world/new', {name: 'newWorld'});

Router.route('/profile/edit/:id', {name: 'editProfile'});
Router.route('/profile/view/:id', {name: 'viewProfile'});
Router.route('/profile', {name: 'profile'});

Router.route('/browse', {name: 'browse'});


Router.route('/about', {name: 'About'});
Router.route('/help', {name: 'Help'});
Router.route('/mobile', {name: 'Mobile'});
Router.route('/premium', {name: 'Premium'});
Router.route('/advertise', {name: 'Company'});
Router.route('/privacy', {name: 'Privacy'});
Router.route('/terms', {name: 'Terms'});