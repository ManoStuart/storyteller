EditChapterController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("singleChapter", this.params.id),
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
    var aux = Chapters.findOne({_id: this.params.id});
    if (aux &&
       (aux.userId != Meteor.userId() ||
        aux.status == 'inactive')) {
      Router.go('home');
      return;
    }
    
    return aux;
  },

  action: function () {
    this.render();
  }
});
