RunShareController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
    Session.set('run', this.params.id);

    return;
  },

  action: function () {
    this.render();
  }
});
