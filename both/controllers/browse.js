BrowseController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
  },

  action: function () {
    this.render();
  }
});
