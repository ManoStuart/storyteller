EditWorldController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("singleWorld", this.params.id),
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
    var aux = Worlds.findOne({_id: this.params.id});
    if (aux && aux.userId != Meteor.userId()) {
      Router.go('home');
      return;
    }
    return aux;
  },

  action: function () {
    this.render();
  }
});
