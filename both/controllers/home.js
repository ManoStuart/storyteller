HomeController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("queryWorlds", {}, {limit: 9, sort: {"views.count": -1}}),
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
  },

  action: function () {
    this.render();
  }
});
