NewWorldController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("latestRun"),
    ];
  },

  data: function () {
  },

  action: function () {
    if (!Meteor.user()) {
      Router.go('home');
      return;
    }
    
    this.render();
  }
});
