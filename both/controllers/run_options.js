RunOptionsController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("runWorld", this.params.id, true),
      Meteor.subscribe("singleRun", this.params.id),
    ];
  },

  data: function () {
    Session.set('run', this.params.id);
    var aux = Runs.findOne({_id: this.params.id});
    
    if (aux) {
      if (aux.userId != Meteor.userId()) {
        Router.go('home');
        return;
      }

      var world = Worlds.findOne({_id: aux.worldId});
      if (world && Meteor.user()) {
        if (world.minLvl > Meteor.user().profile.lvl ||
            (world.viewPermissions.type == 'private' &&
            _.indexOf(world.viewPermissions.allowed, Meteor.userId()) == -1 )) {
          Router.go('home');
        }
      }
    }

    return aux;
  },

  action: function () {
    this.render();
  }
});
