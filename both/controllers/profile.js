ProfileController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("myWorlds"),
      Meteor.subscribe("myChapters"),
      Meteor.subscribe("myRuns"),
      Meteor.subscribe("myLastRunChapters"),
      Meteor.subscribe("myChaptersStats"),
    ];
  },

  data: function () {
    return Meteor.user();
  },

  action: function () {
    if (!Meteor.userId()) {
      Router.go('home');
      return;
    }
    
    this.render();
  }
});
