NewChapterController = RouteController.extend({
  waitOn: function () {
    if (!Session.get('run')) {
      Router.go('home');
    }
    
    return [
      Meteor.subscribe("runWorld", Session.get('run'), false),
      Meteor.subscribe("singleRun", Session.get('run')),
      Meteor.subscribe("newChapterParent", Session.get('run')),
    ];
  },

  data: function () {
    var run = Runs.findOne({_id: Session.get('run')});
    if (run) {
      if (run.userId != Meteor.userId()) {
        Router.go('home');
        return;
      }

      var num = run.history.length -1;
      if (run.history[num].choice != undefined) {
        id = run.history[num].chapter;
      } else {
        num -= 1;
        if (!run.history[num]) {
          return Router.go('home');
        }

        id = run.history[num].chapter;
        run.history.pop();
      }

      run.chapter = Chapters.findOne({_id: id});

      var world = Worlds.findOne({_id: run.worldId});
      if (world && world.writePermissions.type == 'private' &&
        _.indexOf(world.writePermissions.allowed, Meteor.userId()) == -1) {
        Router.go('home');
      }

      return run;
    }
  },

  action: function () {
    this.render();
  }
});
