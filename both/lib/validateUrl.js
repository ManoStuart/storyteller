ValidEmbedUrl = function (url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*)?(&list=)?([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match&&match[7].length==11) {
      var aux = {id: match[7]};
      if (match[9].length==13)
        aux.list = match[9];

        return aux;
    } else {
        return null;
    }
}