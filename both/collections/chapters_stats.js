ChaptersStats = new Meteor.Collection('chapters_stats');

ChaptersStatsSchema = new SimpleSchema({
  // Tagging System
  tags: {
    type: [Object],
    label: "Tags",
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  },
  "tags.$.name": {
    type: String,
    label: "Tag Name"
  },
  "tags.$.users": {
    type: [String],
  },
  "tags.$.count": {
    type: Number
  },
  "tags.$.reports": {
    type: Object
  },
  "tags.$.reports.count": {
    type: Number
  },
  "tags.$.reports.users": {
    type: [String],
    defaultValue: [],
  },


  // Report System
  reports: {
    type: Object,
    autoValue: function() {
      if (this.isInsert) {
        return {
          spam: {count: 0, users: []},
          nsfw: {count: 0, users: []},
          copyright: {count: 0, users: []}
        };
      }
    }
  },
  "reports.spam": {
    type:  Object
  },
  "reports.spam.count": {
    type:  Number,
    defaultValue: 0,
  },
  "reports.spam.users": {
    type:  [String],
    defaultValue: [],
  },
  "reports.nsfw": {
    type:  Object
  },
  "reports.nsfw.count": {
    type:  Number,
    defaultValue: 0,
  },
  "reports.nsfw.users": {
    type:  [String],
    defaultValue: [],
  },
  "reports.copyright": {
    type:  Object
  },
  "reports.copyright.count": {
    type:  Number,
    defaultValue: 0,
  },
  "reports.copyright.users": {
    type:  [String],
    defaultValue: [],
  },


  // Rating System 
  voteCount: {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 1;
      }
    }
  },
  upVotes: {
    type: [String],
    label: "Up Votes",
    autoValue: function() {
      if (this.isInsert) {
        return [this.userId];
      }
    }
  },
  downVotes: {
    type: [String],
    label: "Down Votes",
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  },
  expThreshold: {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 0;
      }
    }
  },

  chapterId: {
    type: String,
    unique: true
  },
  userId: {
    type: String,
    label : "Owner",
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
    }
  }
});

ChaptersStats.attachSchema(ChaptersStatsSchema);