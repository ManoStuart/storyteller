Worlds = new Meteor.Collection('worlds');

WorldsSchema = new SimpleSchema({
  // Content
  name: {
    type: String,
    label : "Name"
  },
  minLvl: {
    type: Number
  },
  startingChapter: {
    type: String,
    label : "Starting Chapter",
    optional: true
  },

  // Permissions
  viewPermissions: {
    type: Object,
    label : "View Permissions",
    defaultValue: {
      type: 'public',
      allowed: []
    }
  },
  "viewPermissions.type": {
    type: String,
    label : "View Permission Type",
    allowedValues: ['public', 'private'],
  },
  "viewPermissions.allowed": {
    type: [String],
    optional: true,
    label : "View Allowed Users",
    defaultValue: [],
  },

  writePermissions: {
    type: Object,
    label : "Write Permissions",
    defaultValue: {
      type: 'public',
      allowed: []
    }
  },
  "writePermissions.type": {
    type: String,
    label : "Write Permission Type",
    allowedValues: ['public', 'private'],
  },
  "writePermissions.allowed": {
    type: [String],
    optional: true,
    label : "Write Allowed Users",
    defaultValue: [],
  },

  // Views
  views: {
    type: Object
  },
  "views.count": {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 0;
      }
    }
  },
  "views.users": {
    type: [String],
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  },

  chaptersCount: {
    type: Number,
    autoValue: function () {
      if (this.isInsert)
        return 0;
    }
  },

  // Tags
  tags: {
    type: [Object],
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  },
  "tags.$.name": {
    type: String
  },
  "tags.$.count": {
    type: Number
  },

  // Control
  userId: {
    type: String,
    label : "Owner",
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }

      // this.unset();
    }
  },
  createdAt: {
    type: Date,
    label : "Created At",
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      }
      
      this.unset();
    }
  }
});

Worlds.attachSchema(WorldsSchema);