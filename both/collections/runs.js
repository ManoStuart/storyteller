Runs = new Meteor.Collection('runs');

RunsSchema = new SimpleSchema({
  // Controls
  userId: {
    type: String,
    label : "Owner"
  },
  worldId: {
    type: String
  },
  createdAt: {
    type: Date,
    label: "Created At",
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  updatedAt: {
    type: Date,
    label: "Updated At",
    autoValue: function() {
      return new Date();
    }
  },

  // Run Options
  name: {
    type: String,
    label : "Name"
  },
  tags: {
    type: [String],
    defaultValue: []
  },
  languages: {
    type: [String],
    defaultValue: []
  },
  nsfw: {
    type: Boolean,
    defaultValue: false
  },
  public: {
    type: Boolean,
    defaultValue: true,
  },
  chapterSelection: {
    type: String,
    defaultValue: 'Top',
    allowedValues: ['Top', 'Random', 'Select'],
  },

  // Chapters related
  history: {
    type: [Object],
  },
  "history.$.chapter": {
    type: String
  },
  "history.$.choice": {
    type: Number,
    optional: true
  },
  "history.$.variables": {
    type: Object,
    optional: true,
    blackbox: true,
  },

  rejected: {
    type: [String],
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  }
});

Runs.attachSchema(RunsSchema);