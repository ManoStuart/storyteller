Comments = new Meteor.Collection('comments');

CommentsSchema = new SimpleSchema({
  chapterId: {
    type: String,
    label: "Parent Chapter Id",
  },
  parentId: {
    type: String,
    label: "Parent Comment Id",
    optional: true,
  },
  createdAt: {
    type: Date,
    label : "Created At",
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  userId: {
    type: String,
    label : "Comment user",
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
      this.unset();
    }
  },
  text: {
    type: String,
    label: "Comment text"
  },

  // Rating System 
  voteCount: {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 1;
      }
    }
  },
  upVotes: {
    type: [String],
    label: "Up Votes",
    autoValue: function() {
      if (this.isInsert) {
        return [this.userId];
      }
    }
  },
  downVotes: {
    type: [String],
    label: "Down Votes",
    autoValue: function() {
      if (this.isInsert) {
        return [];
      }
    }
  },
});

Comments.attachSchema(CommentsSchema);