var UserProfile = new SimpleSchema({
  name: {
    type: String
  },
  location: {
    type: String,
    optional: true
  },
  lvl: {
    type: Number
  },
  exp: {
    type: Number
  },
  picture: {
    type: String,
    optional: true
  },
  views: {
    type: Number
  },
  public: {
    type: Boolean
  },

  images: {
    type: [Object],
    defaultValue: []
  },

  sounds: {
    type: [Object],
    defaultValue: []
  },
  "sounds.$.src": {
    type: String
  },
  "sounds.$.name": {
    type: String
  }
});

var User = new SimpleSchema({
  username: {
    type: String,
    optional: true
  },
  emails: {
    type: [Object]
  },
  "emails.$.address": {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    label: 'Email'
  },
  "emails.$.verified": {
    type: Boolean
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  profile: {
    type: UserProfile,
    optional: true
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },
  roles: {
    type: Object,
    optional: true,
    blackbox: true
  }
});

//Meteor.users.attachSchema(User);