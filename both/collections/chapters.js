Chapters = new Meteor.Collection('chapters');

ChaptersSchema = new SimpleSchema({
  // Content
  name: {
    type: String,
    label : "Name"
  },
  description: {
    type: String,
    label : "Description"
  },
  content: {
    type: String,
    label : "Content"
  },
  variables: {
    type: [Object],
    optional: true,
  },
  "variables.$.name": {
    type: String,
  },
  "variables.$.type": {
    type: String,
    allowedValues: ["Text", "Boolean", "Number"],
  },

  // Tree Vertices
  parent: {
    type: Object,
    optional: true
  },
  "parent.chapter": {
    type: String
  },
  "parent.choice": {
    type: Number
  },

  childChapters: {
    type: Object,
    label: "Child Chapters"
  },
  "childChapters.type": {
    type: String,
    allowedValues: ['random', 'fixed', 'expanding']
  },
  "childChapters.choices": {
    type: [Object],
    defaultValue: [],
  },
  "childChapters.choices.$.name": {
    type: String
  },
  "childChapters.choices.$.users": {
    type: [String],
    autoValue: function() {
      if (this.isInsert) {
        return [this.userId];
      }
    }
  },
  "childChapters.choices.$.count": {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 1;
      }
    }
  },
  "childChapters.choices.$.chaptersCount": {
    type: Number,
    autoValue: function() {
      if (this.isInsert) {
        return 0;
      }
    }
  },

  // Control
  nsfw: {
    type: Boolean
  },
  language: {
    type: String,
    allowedValues: ['en', 'pt', 'ru', 'cn']
  },
  status: {
    type: String,
    allowedValues: ['active', 'inactive'],
    autoValue: function() {
      if (this.isInsert) {
        return 'active';
      }
    }
  },
  userId: {
    type: String,
    label : "Owner",
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      }
      this.unset();
    }
  },
  worldId: {
    type: String
  },
  createdAt: {
    type: Date,
    label : "Created At",
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      }
      this.unset();
    }
  }
});

Chapters.attachSchema(ChaptersSchema);