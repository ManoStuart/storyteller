Meteor.methods({
  getUserId: function (name) {
    return Meteor.users.findOne(
    	{"username": name},
    	{
    		fields: {
    			_id: 1,
    			username: 1,
    		}
    	}
    );
  },
  getPermissionArray: function (ids) {
  	return Meteor.users.find(
    	{_id: {$in: ids}},
    	{
    		fields: {
    			_id: 1,
    			username: 1,
    		}
    	}
    ).fetch();
  }
});