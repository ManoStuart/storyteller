Meteor.methods({
  countChoice: function (num, id) {
    if (!this.userId) return;

    var aux = Chapters.findOne({_id: id});

    if (!aux) return;

    // Array overflow
    if (aux.childChapters.choices.length <= num || num < 0) return;

    var str = "childChapters.choices." + num + ".count";
    var inc = {};
    inc[str] = 1;
    str = "childChapters.choices." + num + ".users";
    var push = {};
    push[str] = this.userId;
    var query = {_id: id};
    query[str] = {$ne: this.userId};

    Chapters.update(query, {'$push': push, $inc: inc});
  },
  newChoice: function (name, id) {
    if (!this.userId) return;

    var aux = Chapters.findOne({_id: id});

    if (!aux) return;

    for (var i = 0; i < aux.childChapters.choices.length; i++) {

      // Choice already exists
      if (aux.childChapters.choices[i].name == name) {
        Meteor.call('countChoice', i, id);
        return;
      }
    }

    Chapters.update(
      {_id: id},
      {
        $push: {
          "childChapters.choices": {
            name: name,
            users: [this.userId],
            count: 1,
            chaptersCount: 0,
          }
        }
      }
    );
  },
  changeChoiceType: function (id, type) {
    var chapter = Chapters.findOne({_id: id});
    if (!chapter) throw new Meteor.Error('404', 'Chapter not found.');

    if (chapter.userId !== this.userId) throw new Meteor.Error('403', 'Not chapter owner.');

    if (! _.contains(['random', 'fixed', 'expanding'], type))
      throw new Meteor.Error('403', 'Invalid choice type');

    Chapters.update({_id: id}, {$set: {"childChapters.type": type}});
  }
});