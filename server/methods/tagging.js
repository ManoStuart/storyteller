Meteor.methods({
  addTag: function (name, id) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});

    if (!aux) return;
    statsId = aux._id;

    // get index
    for (var i = 0; i < aux.tags.length; i++) {
      if (aux.tags[i].name == name) {
        break;
      }
    }

    // New Tag
    if (i == aux.tags.length) {
      var push = {tags: {
        name: name,
        count: 1,
        users: [this.userId],
        reports: {count: 0, users: []}
      }};

      ChaptersStats.update(
        {_id: statsId},
        {'$push': push}
      );

      addWorldTag(name, id);
      return;
    }


    // Existing Tag
    var str = "tags."+ i + ".count";
    var inc = {};
    inc[str] = 1
    var str = "tags."+ i + ".users";
    var push = {};
    push[str] = this.userId;

    var query =  {_id: statsId};
    query[str] = {$ne: this.userId};

    ChaptersStats.update(
      query,
      {'$push': push, '$inc': inc}
    );
  },
  removeTag: function (name, id) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});

    if (!aux) return;
    statsId = aux._id;

    // get index
    for (var i = 0; i < aux.tags.length; i++) {
      if (aux.tags[i].name == name) {
        break;
      }
    }

    // Tag not found
    if (i == aux.tags.length) {
      return;
    }

    // Tag count 0  -- Delete tag
    if (aux.tags[i].count == 1) {
      ChaptersStats.update(
        {_id: statsId},
        {'$pull': {tags: {name: name}}}
      );

      removeWorldTag(name, id);
      return;
    }

    // Existing Tag
    var str = "tags."+ i + ".count";
    var inc = {};
    inc[str] = -1
    var str = "tags."+ i + ".users";
    var pull = {};
    pull[str] = this.userId;

    var query =  {_id: statsId};
    query[str] = this.userId;

    ChaptersStats.update(
      query,
      {'$pull': pull, '$inc': inc}
    );
  },
  toggleReportTag: function (name, id) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});

    if (!aux) return;
    statsId = aux._id;

    // get index
    for (var i = 0; i < aux.tags.length; i++) {
      if (aux.tags[i].name == name) {
        break;
      }
    }

    // Tag not found
    if (i == aux.tags.length) {
      return;
    }

    // Add Report
    if (_.indexOf(aux.tags[i].reports.users, this.userId) == -1) {
      // Reports Reached limit
      if (aux.tags[i].reports.count + 1 > 15 && aux.tags[i].reports.count + 1 > aux.tags[i].count) {
        ChaptersStats.update(
          {_id: statsId},
          {'$pull': {tags: {name: name}}}
        );
      
        removeWorldTag(name, id);
        return;
      }

      var str = "tags."+i+".reports.count";
      var inc = {};
      inc[str] = 1;
      str = "tags."+i+".reports.users";
      var push = {};
      push[str] = this.userId;

      ChaptersStats.update(
        {_id: statsId},
        {$inc: inc, $push: push}
      );
      return;
    }
    
    // Remove Report
    var str = "tags."+i+".reports.count";
    var inc = {};
    inc[str] = -1;
    str = "tags."+i+".reports.users";
    var pull = {};
    pull[str] = this.userId;

    ChaptersStats.update(
      {_id: statsId},
      {$inc: inc, $pull: pull}
    ); 
  }
});

addWorldTag = function (name, id) {
  var chapter = Chapters.findOne({_id: id});
  if (!chapter) return;

  var aux = Worlds.findOne({_id: chapter.worldId});
  if (!aux) return;

  // get index
  for (var i = 0; i < aux.tags.length; i++) {
    if (aux.tags[i].name == name) {
      break;
    }
  }

  // New Tag
  if (i == aux.tags.length) {
    Worlds.update(
      {_id: aux._id},
      {'$push': {tags: {name: name, count: 1}}}
    );

    return;
  }

  // Existing Tag
  var str = "tags."+ i + ".count";
  var inc = {};
  inc[str] = 1

  Worlds.update(
    {_id: aux._id},
    {'$inc': inc}
  );
}

removeWorldTag = function (name, id) {
  var chapter = Chapters.findOne({_id: id});
  if (!chapter) return;

  var aux = Worlds.findOne({_id: chapter.worldId});
  if (!aux) return;

  // get index
  for (var i = 0; i < aux.tags.length; i++) {
    if (aux.tags[i].name == name) {
      break;
    }
  }

  // Tag not found
  if (i == aux.tags.length) {
    return;
  }

  // Tag count 0  -- Delete tag
  if (aux.tags[i].count == 1) {
    Worlds.update(
      {_id: aux._id},
      {'$pull': {tags: {name: name, count: 1}}}
    );

    return;
  }

  // Existing Tag
  var str = "tags."+ i + ".count";
  var inc = {};
  inc[str] = -1

  Worlds.update(
    {_id: aux._id},
    {'$inc': inc}
  );
}