Meteor.methods({
  toggleReportChapter: function (id, type) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});
    if (!aux) return;
    statsId = aux._id;

    // Add Report
    if (_.indexOf(aux.reports[type].users, this.userId) == -1) {
      // Reports Reached limit
      if (checks[type](aux)) {
        return;
      }

      var str = "reports."+type+".count";
      var inc = {};
      inc[str] = 1;
      str = "reports."+type+".users";
      var push = {};
      push[str] = this.userId;

      ChaptersStats.update(
        {_id: statsId},
        {$inc: inc, $push: push}
      );
      return;
    }
    
    // Remove Report
    var str = "reports."+type+".count";
    var inc = {};
    inc[str] = -1;
    str = "reports."+type+".users";
    var pull = {};
    pull[str] = this.userId;

    ChaptersStats.update(
      {_id: statsId},
      {$inc: inc, $pull: pull}
    ); 
  }
});

// Peform actions suited to each kind of report.
var checks = {
  nsfw: function (stats) {
    var chapter = Chapters.findOne({_id: stats.chapterId});
    if (!chapter) return false;

    if (stats.reports.nsfw.count + 1 > 25 && !chapter.nsfw) {
      Chapters.update({_id: chapter._id}, {$set: {nsfw: true}});
      ChaptersStats.update({_id: stats._id}, {$set: {"reports.nsfw": {count: 0, users: []}}});
      return true;
    }

    if (stats.reports.nsfw.count + 1 > 50 && chapter.nsfw) {
      // Shit is really dark
      return false;
    }
  },
  spam: function (stats) {
    if (stats.reports.spam.count + 1 > 150 ||
       (stats.reports.spam.count + 1 > stats.voteCount &&
        stats.reports.spam.count + 1 > 25)) {

      var chapter = Chapters.findOne({_id: stats.chapterId});
      if (!chapter) return false;

      addXp(-100, chapter.userId);
      ChaptersStats.update({_id: stats._id}, {$set: {"reports.spam": {count: 0, users: []}}});

      // Remove content from spamming chapter
      Chapters.update(
        {_id: chapter._id},
        {
          $set: {
            name: "REPOST / SPAM / ADVERTISING",
            description: ' ',
            content: ' ',
            status: 'inactive'
          }
        }
      );

      // Detach child
      if (chapter.parent) {
        var str = "childChapters.choices."+chapter.parent.choice+".chapters";
        var pull = {};
        pull[str] = chapter._id;

        Chapters.update(
          {_id: chapter.parent.chapter},
          {$pull: pull}
        );
      }

      return true;
    }
  },
  copyright: function (stats) {
    if (stats.reports.copyright.count + 1 > 600) {
      var chapter = Chapters.findOne({_id: stats.chapterId});
      if (!chapter) return false;

      addXp(-20, chapter.userId);
      ChaptersStats.update({_id: stats._id}, {$set: {"reports.copyright": {count: 0, users: []}}});

      // Remove content from spamming chapter
      Chapters.update(
        {_id: chapter._id},
        {
          $set: {
            name: "COPYRIGHT INFRINGEMENT",
            description: ' ',
            content: ' ',
            status: 'inactive'
          }
        }
      );

      // Detach child
      if (chapter.parent) {
        var str = "childChapters.choices."+chapter.parent.choice+".chapters";
        var pull = {};
        pull[str] = chapter._id;

        Chapters.update(
          {_id: chapter.parent.chapter},
          {$pull: pull}
        );
      }

      return true;
    }
  }
}