Meteor.methods({
  commentCount: function (query) {
    if (!query) return;

    return Comments.find(query).count();
  },
  upVoteComment: function (id) {
    if (!this.userId) return;

    var aux = Comments.findOne(id);
    if (!aux) return;

    // Remove Vote
    if (_.indexOf(aux.upVotes, this.userId) != -1) {
      Comments.update(
        {_id: id},
        {'$pull': {'upVotes': this.userId}, '$inc': {voteCount: -1}}
      );
      return;
    }

    // Change Vote
    if (_.indexOf(aux.downVotes, this.userId) != -1) {
      Comments.update(
        {_id: id},
        {
          '$pull': {'downVotes': this.userId},
          '$push': {'upVotes': this.userId},
          '$inc': {voteCount: 2}
        }
      );
    } else {
      // First Time Voting
      Comments.update(
        {_id: id},
        {'$push': {'upVotes': this.userId}, '$inc': {voteCount: 1}}
      );
    }
  },
  downVoteComment: function (id) {
    if (!this.userId) return;

    var aux = Comments.findOne(id);
    if (!aux) return;

    // Remove Vote
    if (_.indexOf(aux.downVotes, this.userId) != -1) {
      Comments.update(
        {_id: id},
        {'$pull': {'downVotes': this.userId}, '$inc': {voteCount: 1}}
      );
      return;
    }

    // Change Vote
    if (_.indexOf(aux.upVotes, this.userId) != -1) {
      Comments.update(
        {_id: id},
        {
          '$push': {'downVotes': this.userId},
          '$pull': {'upVotes': this.userId},
          '$inc': {voteCount: -2}
        }
      );
    } else {
      // First Time Voting
      Comments.update(
        {_id: id},
        {'$push': {'downVotes': this.userId}, '$inc': {voteCount: -1}}
      );
    }
  }
});