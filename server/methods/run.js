Meteor.methods({
  selectChapter: function (id, chapterId) {
    var run = Runs.findOne(id);
    if (!run || !run.history || run.history.length < 1) return;

    var id = run.history[run.history.length -1].chapter;
    var num = run.history[run.history.length -1].choice;
    if (num == undefined) return;

    var newChapter = Chapters.findOne(chapterId);
    if (!newChapter || !newChapter.parent || newChapter.parent.chapter !== id ||
        newChapter.parent.choice !== num || newChapter.worldId !== run.worldId)
      return;

    Runs.update({_id: run._id}, {$push: {history: {chapter: chapterId}}});
  },
  randomChapter: function (id) {
    var run = Runs.findOne(id);
    if (!run || !run.history || run.history.length < 1) return;

    var id = run.history[run.history.length -1].chapter;
    var num = run.history[run.history.length -1].choice;
    if (num == undefined) return;

    var query = {
      _id: {$nin: run.rejected},
      status: 'active',
      parent: {
        chapter: id,
        choice: num,
      }
    };

    if (!run.nsfw) query.nsfw = false;

    if (run.languages.length > 0)
      query.language = {$in: run.languages};

    // Filtering by rejected, nsfw and language
    var aux = Chapters.find(query, {fields: {_id : 1}}).fetch();
    if (aux.length == 0) return;

    aux = _.pluck(aux, '_id');

    // Sorting by vote
    aux = ChaptersStats.find(
      {chapterId: {$in: aux}},
      {sort: {voteCount: -1}}
    ).fetch();

    var tagged = {arr: [], sum: 0, min: -1, matches: 0};

    aux.forEach (function(chapter) {
      var tags = _.pluck(chapter.tags, "name");

      // Find how many tags match the prefered tags from run
      var match = _.intersection(tags, run.tags).length;

      // Keep only those that match the most tags
      if (tagged.matches > match) return;
      if (tagged.matches < match) {
        tagged = {arr: [], sum: 0, min: -1, matches: match};
      }

      // Keep track of vote stats for random purposes
      tagged.arr.push(chapter);
      tagged.sum += chapter.voteCount;
      if (tagged.min >= chapter.voteCount) {
        tagged.min = chapter.voteCount - 1;
      }
    });

    var newChapter;

    if (run.chapterSelection === 'Random') {
      // Level the sum so downvoted chapters wont be negleted.
      tagged.sum -= tagged.arr.length * tagged.min;
      var rand = Math.floor((Math.random() * tagged.sum));

      // Check who was randomed
      for (var i = 0; i < tagged.arr.length; i++) {
        rand -= tagged.arr[i].voteCount - tagged.min;
        if (rand < 0) {
          newChapter = tagged.arr[i].chapterId;
          break;
        }
      }
    } else if (run.chapterSelection === 'Top') {
      newChapter = tagged.arr[0].chapterId;
    }

    if (!newChapter) return;

    Runs.update({_id: run._id}, {$push: {history: {chapter: newChapter}}});
  },
  newRun: function (id) {
    var user = Meteor.users.findOne({_id: this.userId});
    if (!user) throw new Meteor.Error(403, 'Login required');

    var chapter = Chapters.findOne({_id: id});
    if (!chapter) throw new Meteor.Error(404, 'Chapter not found');

    var worldError = worldVerify(chapter.worldId, user);
    if (worldError) throw new Meteor.Error(403, worldError);

    try {
      var id = newRunSync({
        name: "New Run",
        history: [{chapter: id}],
        userId: this.userId,
        worldId: chapter.worldId
      });

      Worlds.update(
        {_id: chapter.worldId, "views.users": {$ne: this.userId}},
        {
          $inc: {"views.count": 1},
          $push: {"views.users": this.userId}
        }
      );

      return id;
    }
    catch(error) {
      throw new Meteor.Error(500, error.message);
    }
  },
  copyRun: function (id, nameAdd, index) {
    var user = Meteor.users.findOne({_id: this.userId});
    if (!user) throw new Meteor.Error(403, 'Login required');

    var run = Runs.findOne(id);
    if (!run)
      throw new Meteor.Error('404', 'Run not found');

    if (!run.public && run.userId !== this.userId)
      throw new Meteor.Error('403', 'This run is not public');

    if (index !== false) {
      run.history.splice(index+1, run.history.length-index+1);
      delete run.history[run.history.length-1].choice;
    } else {
      if (run.userId === this.userId) return id;
    }

    var worldError = worldVerify(run.worldId, user);
    if (worldError) throw new Meteor.Error(403, worldError);

    var updateChapers = false;
    if (run.userId !== this.userId)
      updateChapers = true;

    delete run._id;
    run.name += ' ' + nameAdd;
    run.userId = this.userId;
    run.createdAt = new Date();
    run.updatedAt = new Date();
    run.rejected = [];

    try {
      var id = newRunSync(run);

      if (updateChapers) {
        run.history.forEach(function (value) {
          if (value.choice != undefined) {
            Meteor.call('countChoice', value.choice, value.chapter);
          }
        });
      }

      return id;
    }
    catch(error) {
      throw new Meteor.Error(500, error.message);
    }
  },
  newRandomRun: function () {
    var user = Meteor.users.findOne(this.userId);
    if (!user) throw new Meteor.Error(403, "Login required");;

    var query = {
      minLvl: {$lte: user.profile.lvl},
      $or: [
        {"viewPermissions.type": 'public'},
        {"viewPermissions.allowed": Meteor.userId()}
      ]
    }

    var max = Worlds.find(query, {fields: {_id: 1, startingChapter: 1}}).fetch();

    if (max.length == 0)
      throw new Meteor.Error(404, "Couldn't find a world for you");

    var rand = Math.floor((Math.random() * max.length));

    var sel = max[rand];

    var chapter = Chapters.findOne(sel.startingChapter);
    if (!chapter) throw new Meteor.Error(500, "World information corrupted");

    try {
      var id = newRunSync({
        name: "Random Run",
        history: [{chapter: sel.startingChapter}],
        userId: this.userId,
        worldId: sel._id
      });

      Worlds.update(
        {_id: sel._id, "views.users": {$ne: this.userId}},
        {
          $inc: {"views.count": 1},
          $push: {"views.users": this.userId}
        }
      );

      return id;
    }
    catch(error) {
      throw new Meteor.Error(500, error.message);
    }
  },
});

var worldVerify = function (id, user) {
  var world = Worlds.findOne({_id: id});
  if (!world) return 'World not Found';

  // Below required lvl
  if (world.minLvl > user.profile.lvl) return 'Below minimun level';

  // Not allowed to see
  if (world.viewPermissions.type == 'private') {
    if (_.indexOf(world.viewPermissions.allowed, this.userId) != -1) {
      return 'View of this world is private';
    }
  }

  return false;
}


var newRunAsync = function (data, callback) {
  Runs.insert(data, function(error, result) {
    callback(error, result);
  });
}

var newRunSync = Meteor.wrapAsync(newRunAsync);