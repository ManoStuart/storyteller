var thresholds = [
  {value:10, reward:10},
  {value:100, reward:50},
  {value:1000, reward:100},
  {value:10000, reward:100},
  {value:100000, reward:100},
  {value:1000000, reward:100},
];

Meteor.methods({
  upVote: function (id) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});

    if (!aux) return;
    id = aux._id;

    // Remove Vote
    if (_.indexOf(aux.upVotes, this.userId) != -1) {
      // Chapters.update(
      //   {_id: id},
      //   {'$pull': {'upVotes': this.userId}, '$inc': {voteCount: -1}}
      // );
      return;
    }

    // Change Vote
    if (_.indexOf(aux.downVotes, this.userId) != -1) {
      ChaptersStats.update(
        {_id: id},
        {
          '$pull': {'downVotes': this.userId},
          '$push': {'upVotes': this.userId},
          '$inc': {voteCount: 2}
        }
      );
      aux.voteCount += 2;
    } else {
      // First Time Voting
      ChaptersStats.update(
        {_id: id},
        {'$push': {'upVotes': this.userId}, '$inc': {voteCount: 1}}
      );
      Meteor.users.update({_id: this.userId}, {$inc: {"profile.views": 1}});
      addXp(1, this.userId);
      aux.voteCount += 1;
    }

    // EXP given to good posts
    if (aux.expThreshold >= 0 && aux.voteCount > thresholds[aux.expThreshold].value) {
      ChaptersStats.update({_id: id}, {$inc: {expThreshold: 1}});
      addXp(thresholds[aux.expThreshold].reward, aux.userId);
    }
  },
  downVote: function (id) {
    if (!this.userId) return;

    var aux = ChaptersStats.findOne({chapterId: id});

    if (!aux) return;
    id = aux._id;

    // Remove Vote
    if (_.indexOf(aux.downVotes, this.userId) != -1) {
      // Chapters.update(
      //   {_id: id},
      //   {'$pull': {'downVotes': this.userId}, '$inc': {voteCount: 1}}
      // );
      return;
    }

    // Change Vote
    if (_.indexOf(aux.upVotes, this.userId) != -1) {
      ChaptersStats.update(
        {_id: id},
        {
          '$push': {'downVotes': this.userId},
          '$pull': {'upVotes': this.userId},
          '$inc': {voteCount: -2}
        }
      );
      aux.voteCount -= 2;
    } else {
      // First Time Voting
      ChaptersStats.update(
        {_id: id},
        {'$push': {'downVotes': this.userId}, '$inc': {voteCount: -1}}
      );
      Meteor.users.update({_id: this.userId}, {$inc: {"profile.views": 1}});
      addXp(1, this.userId);
      aux.voteCount -= 1;
    }

    // Exp taken from bad posts
    if (aux.expThreshold >= 0 && aux.voteCount < -100) {
      addXp(-50, aux.userId);
      ChaptersStats.update({_id: id}, {$set: {expThreshold: -1}});
    }
  }
});