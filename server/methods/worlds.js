Meteor.methods({
	worldCount: function (query) {
		return Worlds.find(query).count();
	},
  worldChapters: function (id) {
    return Chapters.find({worldId: id}).count();
  },
  getAllTags: function () {
    var worlds = Worlds.find({tags: {$not: {$size: 0}}}, {fields: {tags: 1}}).fetch();

    var aux = _.uniq(_.pluck(_.flatten(_.pluck(worlds, "tags"), true), "name"));

    return aux;
  },
  deleteWorld: function (id) {
    if (this.userId !== 'R45fkjSZjLs6qYiGD') return;

    Chapters.remove({worldId: id}, function (error, result) {
      if (error) console.log(error);
    });

    Worlds.remove({_id: id}, function (error, result) {
      if (error) console.log(error);
    });

    Runs.remove({worldId: id}, function (error, result) {
      if (error) console.log(error);
    })
  },
  deleteChapter: function (id) {
    if (this.userId !== 'R45fkjSZjLs6qYiGD') return;

    Chapters.remove({_id: id});
  },
  setWorldOwner: function (id, target) {
    if (this.userId !== 'R45fkjSZjLs6qYiGD') return;

    Worlds.update({_id: id}, {$set: {userId: target}}, function (error, result) {
      if (error) console.log(error);
    });
  },
});