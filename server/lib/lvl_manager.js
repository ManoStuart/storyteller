addXp = function (num, userId) {
  var aux = Meteor.users.findOne({_id: userId});
  if (!aux) return;

  aux.profile.exp -= num;

  // Lvl up
  if (aux.profile.exp <= 0 ) {
    Meteor.users.update(
      {_id: userId},
      {$inc: {"profile.lvl": 1}, $set: {"profile.exp": 100}}
    );

    return;
  }

  // Lvl down
  if (aux.profile.exp > 100) {
    aux.profile.exp -= 100;
    Meteor.users.update(
      {_id: userId},
      {$inc: {"profile.lvl": -1}, $set: {"profile.exp": aux.profile.exp}}
    );
    // Need to remove gamification bonuses.

    return;
  }

  Meteor.users.update({_id: userId}, {$inc: {"profile.exp": -num}});
}