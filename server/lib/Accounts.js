Accounts.onCreateUser(function(options, user) {
  if (options.profile) {
    user.profile = options.profile;
  } else {
    user.profile = {};
  }

  user.profile.views = 0;
  user.profile.exp = 100;
  user.profile.lvl = 1;
  user.profile.public = true;
  user.profile.sounds = [
    {name: 'Yamete', src: '/sounds/yamete.mp3'},
    {name: 'Applause', src: '/sounds/applause.mp3'}
  ];

  if (user.services && user.services.facebook) {
    var aux = user.services.facebook;
    user.profile.name = aux.first_name;
    user.profile.picture = "http://graph.facebook.com/" + aux.id + "/picture/?type=large";

    if (aux.email) {
      user.emails = [{address: aux.email, verified: true}];
    }
  } else {
    user.profile.name = user.username;
  }

  return user;
});