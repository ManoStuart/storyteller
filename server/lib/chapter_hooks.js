Meteor.startup(function () {
  Chapters.after.insert(function (userId, doc) {
    ChaptersStats.insert({chapterId: this._id});

    if (doc.parent && doc.parent.chapter) {
      var inc = {};
      str = "childChapters.choices." + doc.parent.choice +".chaptersCount";
      inc[str] = 1;
      
      Chapters.update({_id: doc.parent.chapter}, {$inc: inc});
    }
    
    Worlds.update({_id: doc.worldId}, {$inc: {chaptersCount: 1}});
  });

  Chapters.before.update(function (userId, doc, fieldNames, modifier, options) {
    if (modifier.$set && modifier.$set.content) {
      modifier.$set.content = sanitizeContent(modifier.$set.content);
    }

    //remove other modifiers that can change content
  });

  Chapters.before.insert(function (userId, doc) {
    doc.content = sanitizeContent(doc.content);
  });
});

var sanitizeContent = function (dirty) {
  var sanitize = Meteor.npmRequire('sanitize-html');
  
  var allowedAttributes = {
    iframe: ['src', 'class', 'allowfullscreen', 'frameborder'],
    img: ['src', 'class', 'id', 'style'],
    a: [ 'href', 'target'],
    table: ['class'],
  }

  var allowedClasses = {
    img: ['img-thumbnail', 'img-circle', 'img-rounded', 'play-sound'],
    table: ['table', 'table-bordered'],
    iframe: ['summernote-iframe', 'img-thumbnail'],
  }

  var transformTags = {
    'img': imgTransform,
    'iframe': iframeTransform,
  }

  var textTags = sanitize.defaults.allowedTags.concat(['span', 'h1', 'h2']);
  textTags.forEach(function (value) {
    if (value === 'a' || value === 'table') return;

    transformTags[value] = textTransform;
    allowedAttributes[value] = ['class', 'style'];
  });

  var clean = sanitize(dirty, {
    allowedTags: sanitize.defaults.allowedTags.concat(['img', 'iframe', 'span', 'h1', 'h2']),
    allowedAttributes: allowedAttributes,
    allowedSchemes: ['http', 'https', 'data'],
    allowedClasses: allowedClasses,
    transformTags: transformTags,
  });

  return clean;
}

var textTransform = function (tagName, attribs) {
  if (attribs && attribs.style) {
    var regex = /font-family: Arial;|font-style: italic;|font-weight: bold;|text-decoration: underline;|text-align: left;|text-align: center;|text-align: right;|text-align: justify;/ig;

    var styles = attribs.style.match(regex);
    if (styles && styles.length > 0)
      attribs.style = styles.join(' ');
    else
      delete attribs.style;
  }

  var data = {tagName: tagName, attribs: {}};
  if (attribs && attribs.style) data.attribs.style = attribs.style;

  return data;
}

var imgTransform = function (tagName, attribs) {
  if (attribs && attribs.style) {
    var regex = /width: 100%;|width: 50%;|width: 25%;|float: right;|float: left;|float: none;/ig;

    var styles = attribs.style.match(regex);
    if (styles && styles.length > 0)
      attribs.style = styles.join(' ');
    else
      delete attribs.style;
  }

  var data = {tagName: tagName, attribs: {}};
  if (attribs && attribs.class) data.attribs.class = attribs.class;
  if (attribs && attribs.style) data.attribs.style = attribs.style;
  if (attribs && attribs.src) data.attribs.src = attribs.src;
  if (attribs && attribs.id) data.attribs.id = attribs.id;

  return data;
}

var iframeTransform = function (tagName, attribs) {
  var data = {
    tagName: 'iframe',
    attribs: {
      class: 'summernote-iframe img-thumbnail',
      src: attribs.src,
      allowfullscreen: true,
      frameborder: "0",
    }
  }

  if (attribs && attribs.src) {
    var aux = ValidEmbedUrl(attribs.src);
    if (aux === null)
      data.tagName = "Error";
    else
      data.attribs.src = 'https://www.youtube.com/embed/' + aux.id + ((aux.list) ? '?list=' + aux.list : '');
  }

  return data;
}