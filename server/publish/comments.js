Meteor.publish('comments', function (query, modifiers) {
  // var sub = this;

	if (!query) query = {};

	if (!modifiers) modifiers = {limit: 5};

  return Comments.find(query, modifiers);

  // cursor.observeChanges({
  //   added: function (id, doc) {
  //     sub.added('comments', id, transform(doc));
  //   },
  //   changed: function (id, doc) {
  //     sub.changed('comments', id, transform(doc));
  //   },
  //   removed: function (id) {
  //     sub.removed(id);
  //   }
  // });

  // // making sure to mark subscription as ready
  // sub.ready();
});

Meteor.publish('mycomments', function (id) {
  // var sub = this;

	var run = Runs.findOne(id);
	if (!run || !run.history) return [];

  var id = run.history[run.history.length -1].chapter;
  if (!id) return [];

  return Comments.find({userId: this.userId, chapterId: id});

  // cursor.observeChanges({
  //   added: function (id, doc) {
  //     sub.added('comments', id, transform(doc));
  //   },
  //   changed: function (id, doc) {
  //     sub.changed('comments', id, transform(doc));
  //   },
  //   removed: function (id) {
  //     sub.removed(id);
  //   }
  // });

  // // making sure to mark subscription as ready
  // sub.ready();
});

// function that transform the document and create the summary field
function transform (comment) {
	var user = Meteor.users.findOne({_id: comment.userId});
	if (!user) return;

	// insert here name customization
	comment.userName = user.username;

  return comment;
}