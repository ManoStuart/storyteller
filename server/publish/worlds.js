Meteor.publish('worlds', function () {
  return Worlds.find({});
});

Meteor.publish('myWorlds', function () {
  var cursor = Worlds.find(
  	{userId: this.userId},
  	{
  		fields: {
  			name: 1,
  			minLvl: 1,
  			startingChapter: 1,
  			userId: 1,
  			createdAt: 1,
  			"views.count": 1,
        chaptersCount: 1,
  		}
  	}
  );

  cursorTransform(cursor, this, this.userId);
});

Meteor.publish('runWorld', function (id, tags) {
	var run = Runs.findOne(id);
	if (!run) return;

  var fields = {
    _id: 1,
    minLvl: 1,
    viewPermissions: 1,
    writePermissions: 1,
  }
  if (tags) fields.tags = 1;

  var cursor = Worlds.find({
    _id: run.worldId,
    $or: [
      {"viewPermissions.allowed": this.userId},
      {"viewPermissions.type": 'public'},
    ],
  }, {
    fields: fields
  });

  cursorTransform(cursor, this, this.userId);
});

Meteor.publish('singleWorld', function (id) {
  return Worlds.find({
    _id: id,
    $or: [
      {"viewPermissions.allowed": this.userId},
      {"viewPermissions.type": 'public'},
    ],
  }, {
    fields: {
      _id: 1,
      name: 1,
      minLvl: 1,
      viewPermissions: 1,
      writePermissions: 1,
      userId: 1,
    }
  });
});

Meteor.publish('queryWorlds', function (query, modifiers) {
  if (!query) query = {};
  if (!modifiers) modifiers = {limit: 9};

  query.$or = [
    {"viewPermissions.allowed": this.userId},
    {"viewPermissions.type": 'public'},
  ]

  modifiers.fields = {
    userId: 0,
    "views.users": 0,
  }

  var cursor = Worlds.find(query, modifiers);

  cursorTransform(cursor, this, this.userId);
});

// function that transform the document and create the summary field
function transform (world, userId) {
  if (world.startingChapter) {
    var chapter = Chapters.findOne(world.startingChapter);
    if (chapter)
      world.chapterDescription = chapter.description;
  }

  if (world.viewPermissions) {
    filterArray(world.viewPermissions, 'allowed', userId);
  }

  if (world.writePermissions) {
    filterArray(world.writePermissions, 'allowed', userId);
  }

  return world;
}

function filterArray (obj, path, value) {
  if (_.indexOf(obj[path], value) != -1)
    obj[path] = [value];
  else
    obj[path] = [];
}

function cursorTransform (cursor, sub, userId) {
  cursor.observe({
    added: function (document) {
      sub.added('worlds', document._id, transform(document, userId));
    },
    changed: function (newDocument, oldDocument) {
      sub.changed('worlds', newDocument._id, transform(newDocument, userId));
    },
  });

  // making sure to mark subscription as ready
  sub.ready();
}