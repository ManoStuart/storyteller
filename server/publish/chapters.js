Meteor.publish('chapters', function () {
  return Chapters.find({});
});

Meteor.publish('myLastRunChapters', function () {
  var runs = Runs.find({userId: this.userId}, {fields: {history: 1}}).fetch();

  var ids = _.map(runs, function (value) {
    if (!value.history || value.history.length < 1) return '';

    return value.history[value.history.length -1].chapter;
  });

  var cursor = Chapters.find(
    {_id: {$in: ids}},
    {
      fields: {
        _id: 1,
        name: 1,
        description: 1,
        childChapters: 1,
        userId: 1,
        createdAt: 1,
      }
    }
  );

  cursorTransform(cursor, this, this.userId, false);
});

Meteor.publish('myChapters', function () {
  var cursor = Chapters.find(
    {userId: this.userId, status: 'active'},
    {
      fields: {
        _id: 1,
        name: 1,
        description: 1,
        childChapters: 1,
        userId: 1,
        createdAt: 1,
      }
    }
  );

  cursorTransform(cursor, this, this.userId, false);
});

Meteor.publish('singleChapter', function (id) {
  return Chapters.find({_id: id});
});

Meteor.publish('runChapters', function (id) {
	var run = Runs.findOne(id);
	if (!run) return;

	var ids = _.pluck(run.history, "chapter");

  var cursor = Chapters.find(
  	{_id: {$in: ids}},
  	{
  		fields: {
  			name: 1,
  			description: 1,
  			content: 1,
  			childChapters: 1,
  		}
  	}
  );

  cursorTransform(cursor, this, this.userId, false);
});

Meteor.publish('newChapterParent', function (id) {
    var run = Runs.findOne({_id: id});
    if (!run || run.userId !== this.userId) return;

    var num = run.history.length -1;
    if (run.history[num].choice != undefined) {
      id = run.history[num].chapter;
    } else {
      num -= 1;
      if (!run.history[num]) return;

      id = run.history[num].chapter;
    }

    run.chapter = Chapters.find({_id: id});

    var cursor = Chapters.find(
      {_id: id},
      {
        fields: {
          name: 1,
          description: 1,
          content: 1,
          childChapters: 1,
          variables: 1,
        }
      }
    );

    cursorTransform(cursor, this, this.userId, false);
});

Meteor.publish('lastRunChapters', function (id, choice) {
	var query = {
		_id: id,
	};

	var mod = {};
  var vote = false;

  if (choice != undefined) {
    query = {
      parent: {
        chapter: id,
        choice: choice,
      },
      status: 'active',
    }

  	mod = {fields: {_id: 1, name: 1, description: 1, childChapters: 1}};
    vote = true;
  }

  var cursor = Chapters.find(query, mod);

  cursorTransform(cursor, this, this.userId, vote);
});

// function that transform the document and create the summary field
function transform (chapter, userId, vote) {
  if (vote) {
    var stats = ChaptersStats.findOne({chapterId: chapter._id});
    if (stats) chapter.voteCount = stats.voteCount;
  }

	if (!chapter.childChapters || !chapter.childChapters.choices) return chapter;

	chapter.childChapters.choices.forEach (function (value, index) {
		filterArray(value, 'users', userId);

		delete value.chapters;
	});

  return chapter;
}

function filterArray (obj, path, value) {
	if (_.indexOf(obj[path], value) != -1)
		obj[path] = [value];
	else
		obj[path] = [];
}

function cursorTransform (cursor, sub, userId, vote) {
  cursor.observe({
    added: function (document) {
      sub.added('chapters', document._id, transform(document, userId, vote));
    },
    changed: function (newDocument, oldDocument) {
      sub.changed('chapters', newDocument._id, transform(newDocument, userId, vote));
    },
  });

  // making sure to mark subscription as ready
  sub.ready();
}