Meteor.publish('users', function () {
  return Meteor.users.find({});
});

Meteor.publish('usersList', function (idList) {
	if (!idList) return;

  return Meteor.users.find(
  	{_id: {$in: idList}},
  	{
  		fields: {
  			username: 1,
  		}
  	}
  );
});