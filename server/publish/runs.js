Meteor.publish('runs', function () {
  return Runs.find({});
});

Meteor.publish('singleRun', function (id) {
  return Runs.find({_id: id});
});

Meteor.publish('myRuns', function (id) {
  return Runs.find(
    {userId: this.userId},
    {
      fields: {
        name: 1,
        userId: 1,
        updatedAt: 1,
        history: 1,
      }
    }
  );
});

Meteor.publish('latestRun', function () {
  return Runs.find(
  	{userId: this.userId},
  	{
  		fields: {
  			_id: 1,
  			userId: 1,
  			updatedAt: 1,
  		},
  		sort: {updatedAt: -1},
  		limit: 1,
  	}
  );
});