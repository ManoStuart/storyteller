Meteor.publish('chaptersStats', function () {
  return ChaptersStats.find({});
});

Meteor.publish('myChaptersStats', function () {
  return ChaptersStats.find(
  	{userId: this.userId},
  	{
  		fields: {
  			chapterId: 1,
  			voteCount: 1,
  			userId: 1,
  		}
  	}
  );
});

Meteor.publish('lastRunChaptersStats', function (id, choice) {
  var query = {
    chapterId: id,
  };

  var mod = {fields: {expThreshold: 0}};

  if (choice != undefined) {
    var chapters = Chapters.find({
      parent: {
        chapter: id,
        choice: choice,
      },
      status: 'active',
    },{
      fields: {_id: 1}
    }).fetch();

    query.chapterId = {$in: _.pluck(chapters, '_id')};
    mod = {fields: {voteCount: 1, chapterId: 1}};
  }

  var cursor = ChaptersStats.find(query, mod);

  cursorTransform (cursor, this, this.userId);
});

// function that transform the document and create the summary field
function transform (stats, userId) {
	if (stats.tags) {
		stats.tags.forEach (function (value) {
			filterArray(value, 'users', userId);
			filterArray(value, 'reports.users', userId);
			delete value.reports.count;
		});
	}

	if (stats.reports) {
		filterArray(stats, 'reports.spam.users', userId);
		filterArray(stats, 'reports.nsfw.users', userId);
		filterArray(stats, 'reports.copyright.users', userId);

		delete stats.reports.spam.count;
		delete stats.reports.nsfw.count;
		delete stats.reports.copyright.count;
	}

	filterArray(stats, 'downVotes', userId);
	filterArray(stats, 'upVotes', userId);

  return stats;
}

function filterArray (obj, path, value) {
	if (_.indexOf(obj[path], value) != -1)
		obj[path] = [value];
	else
		obj[path] = [];
}

function cursorTransform (cursor, sub, userId) {
  cursor.observe({
    added: function (document) {
      sub.added('chapters_stats', document._id, transform(document, userId));
    },
    changed: function (newDocument, oldDocument) {
      sub.changed('chapters_stats', newDocument._id, transform(newDocument, userId));
    },
  });

  // making sure to mark subscription as ready
  sub.ready();
}