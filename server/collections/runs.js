Runs.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Runs.deny({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    if (userId !== doc.userId) return true;

    if (_.intersection(fieldNames, ['createdAt', 'userId', 'worldId', '_id']).length > 0)
      return true;

    return false;
  },

  remove: function (userId, doc) {
    if (userId !== doc.userId) return true;

    return false;
  },

  fetch: ['userId'],
});
