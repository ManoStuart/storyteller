/*
 * Add query methods like this:
 *  ChaptersStats.findPublic = function () {
 *    return ChaptersStats.find({is_public: true});
 *  }
 */

ChaptersStats.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

ChaptersStats.deny({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  },
  
  fetch: [],
});
