/*
 * Add query methods like this:
 *  Worlds.findPublic = function () {
 *    return Worlds.find({is_public: true});
 *  }
 */

Worlds.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Worlds.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    if (userId !== doc.userId) return true;

    if (_.contains(fieldNames, 'startingChapter') && doc.startingChapter !== undefined)
      return true;

    if (_.intersection(
      fieldNames,
      ['createdAt', 'userId', 'tags', '_id', 'chaptersCount', 'views']
    ).length > 0)
      return true;

    return false;
  },

  remove: function (userId, doc) {
    return true;
  },
  
  fetch: ['userId', 'startingChapter'],
});
