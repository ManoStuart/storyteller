/*
 * Add query methods like this:
 *  Chapters.findPublic = function () {
 *    return Chapters.find({is_public: true});
 *  }
 */

Chapters.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Chapters.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    if (userId !== doc.userId) return true;

    if (_.intersection(
      fieldNames,
      ['createdAt', 'userId', 'worldId', '_id', 'status', 'childChapters', 'parent', 'variables']
    ).length > 0)
      return true;

    return false;
  },

  remove: function (userId, doc) {
    return true;
  },
  
  fetch: ['userId'],
});