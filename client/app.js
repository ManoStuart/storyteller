/*****************************************************************************/
/* Client App Namespace  */
/*****************************************************************************/
_.extend(App, {
});

App.helpers = {
  prettifyDate: function(date) {
    var newDate = moment(date);
    return newDate.format("MMMM Do YYYY, h:mm a");
  },
  getUserName: function (id) {
    var user = Meteor.users.findOne(id);

    if (user) return user.username;
  },
};

_.each(App.helpers, function (helper, key) {
  Handlebars.registerHelper(key, helper);
});
