Meteor.startup(function () {
  Accounts.ui.config({
    requestPermissions: {
      facebook: ['email']
      // Twitter not supported
    },
    passwordSignupFields: 'USERNAME_ONLY'
  });
});