validate = {
  newChapterForm: function () {
    $( "#new-chapter" ).validate({
      rules: {
        "chapter-name": {
          required: true
        },
        "chapter-description": {
          required: true
        }
      }
    });

    $('.note-modal-form').each( function() {$(this).validate({})});
  },
  newWorldForm: function () {
    $( "#new-world" ).validate({
      rules: {
        "chapter-name": {
          required: true
        },
        "chapter-description": {
          required: true
        },
        "world-name": {
          required: true
        },
        "world-lvl": {
          required: true,
          number: true,
          min: 0,
          max: Meteor.user().profile.lvl,
        }
      }
    });

    $('.note-modal-form').each( function() {$(this).validate({})});
  },
  runOptionsForm: function () {
    $( "#run-options" ).validate({
      rules: {
        "run-name": {
          required: true
        }
      }
    });
  },
};

$.validator.setDefaults({ ignore: ":hidden"});