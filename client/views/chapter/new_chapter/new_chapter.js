Template.NewChapter.events({
  'submit #new-chapter': function(e,t) {
    e.preventDefault();
    
    var history = t.data.history;
    var choice = history[history.length -1].choice;

    chapterData = {
      name: $('#chapter-name').val(),
      description: $('#chapter-description').val(),
      nsfw: ($('#nsfw').attr('class') == 'fa fa-2x fa-toggle-off') ? false : true,
      language: $('#chapter-language').val(),
      childChapters: {
        type: $('#choice-type').val(),
        choices: []
      },
      variables: [],
      parent: {
        chapter: t.data.chapter._id,
        choice: choice
      },
      worldId: t.data.worldId,
    };

    // Content Validation
    var contentType = $('#chapter-content-type').val();

    if (contentType === 'image') {
      var image = $('#content-image');

      if (image.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = image.css('width', '100%').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'video') {
      var video = $('#content-video');

      if (video.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = video.removeAttr('style').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'text') {
      chapterData.content = $('#summernote').code();
    }

    if (chapterData.content === '') {
      $('#content-container').addClass('error');
      $('#error-newchapter').text('This field is required.');

      if (contentType === 'image')      
        $('#content-image-input').focus();
      else if (contentType === 'video')
        $('#content-video-input').focus();
      else if (contentType === 'text')
        $('.note-editable').focus();

      Meteor.setTimeout(function() {
        $("#error-newchapter").text('');
        $(".note-editor").toggleClass('error');
      }, 5000);

      return false;
    }

    var aux = Session.get('choices');
    aux.forEach(function(value) {
      chapterData.childChapters.choices.push({name: value.value, users: [Meteor.userId()]});
    });

    aux = Session.get('variables');
    aux.forEach(function(value) {
      chapterData.variables.push({name: value.value, type: value.tooltip});
    });

    Chapters.insert(chapterData, function (error, chapterId) {
      if (error) {
        console.log(error);
        return;
      }
      
      Meteor.call('selectChapter', Session.get('run'), chapterId, function (err, result) {
        Router.go("run", {id: Session.get('run')});
      });
    });
  }
});

Template.NewChapter.helpers({
  lastChoice: function() {
    if (this.history) {
      var num = this.history[this.history.length -1].choice;
      if (num != undefined) {
        return this.chapter.childChapters.choices[num].name;
      }
    }
  },
});

Template.NewChapter.rendered = function () {
  validate.newChapterForm();
};

varColors = {
  Text: "#3c763d",
  Boolean: "#a94442",
  Number: "#31708f",
}

Template.NewChapterFields.events({
  'change #choice-type': function (e,t) {
    if (e.currentTarget.value == 'expanding')
      $('#choice-msg').text('Users can create new choices.');
    else if (e.currentTarget.value == 'fixed')
      $('#choice-msg').text('Choices are pre-defined by you');
    else if (e.currentTarget.value == 'random')
      $('#choice-msg').text('Choices are pre-defined by you, and Users get a random choice when advancing.');
  },
  'keydown #choices-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("choices");

    var input = $('#choices-input').val();
    if (input == '') return false;

    input = toTitleCase(input);

    for (var i = 0; i < aux.length; i++) {
      if (aux[i].value == input) {
        return false;
      }
    }

    aux.push({
      value: input,
      key: input,
      name: "choices"
    });
    Session.set("choices", aux);

    $('#choices-input').val('');

    return false;
  },
  'keydown #variable-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("variables");

    var input = $('#variable-input').val();
    if (input == '') return false;

    input = toTitleCase(input);

    for (var i = 0; i < aux.length; i++) {
      if (aux[i].value == input) {
        return false;
      }
    }

    aux.push({
      value: input,
      key: input,
      name: "variables",
      tooltip: t.$('#variable-type').val(),
      color: varColors[t.$('#variable-type').val()],
    });
    Session.set("variables", aux);

    $('#variable-input').val('');

    return false;
  },
  'click .fa-toggle-off': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-on";
  },
  'click .fa-toggle-on': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-off";
  },
  'click .sound-input': function (e,t) {
    var src = Meteor.user().profile.sounds[e.currentTarget.id];
    // var src = e.currentTarget.id;
    if (!src) return;
    else src = src.src;

    var audioElement = document.createElement('audio');
    audioElement.id = 'sound-'+src;

    var source = document.createElement('source');
    source.type= 'audio/mpeg';
    source.src= src;
    audioElement.appendChild(source);

    var img = document.createElement('img');
    img.src = '/images/play.png';
    img.id = src;
    img.className = 'play-sound'

    $('.note-editable').append(img);
    $('.note-editable').append(audioElement);
  },
  'click .play-sound': function (e,t) {
    var audio = document.getElementById('sound-' + e.currentTarget.id);
    audio.play();
  },
  'change #chapter-content-type': function (e,t) {
    hideContentInput();

    if (e.currentTarget.value == 'image') {
      $('#content-image-input').show();
      $('#content-image').show();
    } else if (e.currentTarget.value == 'video') {
      $('#content-video-input').show();
      $('#content-video').show();
    } else if (e.currentTarget.value == 'text') {
      $('#content-text').show();
    }
  },
  'keydown #content-video-input': function (e,t) {
    if (e.keyCode != 13) return;

    var aux = ValidEmbedUrl(e.currentTarget.value);
    if (aux === null) {
      OpenInfoModal('Please enter a valid youtube url', 'alert alert-danger');
    } else {
      $('#content-video').attr(
        'src',
        'https://www.youtube.com/embed/' + aux.id + ((aux.list) ? '?list=' + aux.list : '')
      );
    }

    return false;
  },
  'change #content-image-input': function (e,t) {
    var reader = new FileReader();

    reader.onload = function (event) {
      $('#content-image').attr('src', event.target.result).css("min-height", "");
    };

    var files = $("#content-image-input")[0].files;
    if (files.length > 0)
      reader.readAsDataURL(files[0]);
    else
      $('#content-image').attr('src', '').css("min-height", "440px");
  },
});

var hideContentInput = function () {
  $('#content-text').hide();
  $('#content-image-input').hide();
  $('#content-image').hide();
  $('#content-video-input').hide();
  $('#content-video').hide();
}

Template.NewChapterFields.helpers({
  hasVariables: function () {
    return (Session.get('variables').length > 0) ? '' : 'hidden';
  },
  hasChoices: function () {
    return (Session.get('choices').length > 0) ? '' : 'hidden';
  },
  hasSounds: function () {
    var user = Meteor.user();
    if (user && user.profile.sounds.length > 0) {
      return true;
    }
  },
  getSounds: function () {
    var user = Meteor.user();
    if (user) {
      user.profile.sounds.forEach(function(value, index) {
        value.index = index;
      });

      return user.profile.sounds;
    }
  },
  setUp: function () {
    if (this.chapter && this.chapter.variables) {
      var aux = [];
      this.chapter.variables.forEach(function (value) {
        aux.push({
          value: value.name,
          key: value.name,
          tooltip: value.type,
          color: varColors[value.type],
          name: 'variables',
        });
      });
      Session.set('variables', aux);
    }
  }
});

Template.NewChapterFields.created = function () {
  Session.set('choices', []);
  Session.set('variables', []);
};

Template.NewChapterFields.rendered = function () {
  $('#summernote').summernote({
    height: 400,
    width: 800,
    minHeight: null,
    maxHeight: null,
    focus: false,
    toolbar: [
      ['fontsize', ['style']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      // ['insert', ['picture', 'video', 'link', 'hr']],
      ['insert', ['picture', 'link', 'hr']],
    ]
  });

  $('.note-toolbar.btn-toolbar > .note-insert.btn-group')
    .prepend($('#add-sound').removeClass('hidden'))
    .prepend($('#add-sound-dropdown'));

  $('.selectpicker').selectpicker('render');
};

Template.NewChapterFields.destroyed = function () {
  delete Session.keys['choices'];
  delete Session.keys['variables'];
};