
/*****************************************************************************/
/* EditChapter: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.EditChapter.events({
  'change #choice-type': function (e,t) {
    Session.set('curChoiceType', e.currentTarget.value);
  },
  'click .fa-toggle-off': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-on";
  },
  'click .fa-toggle-on': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-off";
  },
  'submit #new-chapter': function(e,t) {
    e.preventDefault();

    chapterData = {
      name: $('#chapter-name').val(),
      description: $('#chapter-description').val(),
      nsfw: ($('#nsfw').attr('class') == 'fa fa-2x fa-toggle-off') ? false : true,
      language: $('#chapter-language').val(),
    };

    // Content Validation
    var contentType = $('#chapter-content-type').val();

    if (contentType === 'image') {
      var image = $('#content-image');

      if (image.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = image.css('width', '100%').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'video') {
      var video = $('#content-video');

      if (video.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = video.removeAttr('style').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'text') {
      chapterData.content = $('#summernote').code();
    }

    if (chapterData.content === '') {
      $('#content-container').addClass('error');
      $('#error-newchapter').text('This field is required.');

      if (contentType === 'image')      
        $('#content-image-input').focus();
      else if (contentType === 'video')
        $('#content-video-input').focus();
      else if (contentType === 'text')
        $('.note-editable').focus();

      Meteor.setTimeout(function() {
        $("#error-newchapter").text('');
        $(".note-editor").toggleClass('error');
      }, 5000);

      return false;
    }

    var chapterId = t.data._id;
    var choiceType = $('#choice-type').val();

    Chapters.update({_id: chapterId}, {$set: chapterData}, function(error) {
      if (error) console.log(error);

      Meteor.call('changeChoiceType', chapterId, choiceType, function (e) {
        if (e) console.log(e);
      });

      Router.go('profile', {id: Meteor.userId()});
    });
  },
  'click .sound-input': function (e,t) {
    var src = Meteor.user().profile.sounds[e.currentTarget.id];
    // var src = e.currentTarget.id;
    if (!src) return;
    else src = src.src;

    var audioElement = document.createElement('audio');
    audioElement.id = 'sound-'+src;

    var source = document.createElement('source');
    source.type= 'audio/mpeg';
    source.src= src;
    audioElement.appendChild(source);

    var img = document.createElement('img');
    img.src = '/images/play.png';
    img.id = src;
    img.className = 'play-sound'

    $('.note-editable').append(img);
    $('.note-editable').append(audioElement);
  },
  'click .play-sound': function (e,t) {
    var audio = document.getElementById('sound-' + e.currentTarget.id);
    audio.play();
  },
  'change #chapter-content-type': function (e,t) {
    hideContentInput();

    if (e.currentTarget.value == 'image') {
      $('#content-image-input').show();
      $('#content-image').show();
    } else if (e.currentTarget.value == 'video') {
      $('#content-video-input').show();
      $('#content-video').show();
    } else if (e.currentTarget.value == 'text') {
      $('#content-text').show();
    }
  },
  'keydown #content-video-input': function (e,t) {
    if (e.keyCode != 13) return;

    var aux = ValidEmbedUrl(e.currentTarget.value);
    if (aux === null) {
      OpenInfoModal('Please enter a valid youtube url', 'alert alert-danger');
    } else {
      $('#content-video').attr(
        'src',
        'https://www.youtube.com/embed/' + aux.id + ((aux.list) ? '?list=' + aux.list : '')
      );
    }

    return false;
  },
  'change #content-image-input': function (e,t) {
    var reader = new FileReader();

    reader.onload = function (event) {
      $('#content-image').attr('src', event.target.result).css("min-height", "");
    };

    var files = $("#content-image-input")[0].files;
    if (files.length > 0)
      reader.readAsDataURL(files[0]);
    else
      $('#content-image').attr('src', '').css("min-height", "440px");
  },
});

var hideContentInput = function () {
  $('#content-text').hide();
  $('#content-image-input').hide();
  $('#content-image').hide();
  $('#content-video-input').hide();
  $('#content-video').hide();
}

Template.EditChapter.helpers({
  startShit: function () {
    if (this.language && this.content && this.childChapters && this.childChapters.type) {
      var data = this;
      Meteor.setTimeout(function() {
        $('.selectpicker').selectpicker('refresh');
        $('#summernote').summernote({
          height: 400,
          width: 800,
          minHeight: null,
          maxHeight: null,
          focus: false,
          toolbar: [
            ['fontsize', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['picture', 'link', 'hr']],
          ]
        });

        $('.note-toolbar.btn-toolbar > .note-insert.btn-group')
          .prepend($('#add-sound').removeClass('hidden'))
          .prepend($('#add-sound-dropdown'));

        Session.set('curChoiceType', data.childChapters.type);
      }, 400);
    }
  },
  getNSFW: function() {
    if (this.nsfw) {
      return 'fa-toggle-on';
    }

    return 'fa-toggle-off';
  },
  selectedLanguage: function (lang) {
    if (this.language && this.language == lang) {
      return 'selected';
    }
  },
  selectedChoiceType: function (type) {
    if (this.childChapters && this.childChapters.type == type) {
      return 'selected';
    }
  },
  hasSounds: function () {
    var user = Meteor.user();
    if (user && user.profile.sounds.length > 0) {
      return true;
    }
  },
  getSounds: function () {
    var user = Meteor.user();
    if (user) {
      user.profile.sounds.forEach(function(value, index) {
        value.index = index;
      });

      return user.profile.sounds;
    }
  },
  choiceMsg: function () {
    if (Session.get('curChoiceType') === 'expanding')
      return 'Users can create new choices.';
    else if (Session.get('curChoiceType') === 'fixed')
      return 'Choices are pre-defined by you.';
    else if (Session.get('curChoiceType') === 'random')
      return 'Choices are pre-defined by you, and Users get a random choice when advancing.';
  },
});

/*****************************************************************************/
/* EditChapter: Lifecycle Hooks */
/*****************************************************************************/
Template.EditChapter.created = function () {
};

Template.EditChapter.rendered = function () {
  validate.newChapterForm();
};

Template.EditChapter.destroyed = function () {
  delete Session.keys['curChoiceType'];
};


