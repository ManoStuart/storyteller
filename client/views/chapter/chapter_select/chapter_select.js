Template.ChapterSelect.events({
  'submit #name-form': function (e,t) {
    e.preventDefault();

    var name = t.$('#name-input').val();
    if (name === '') return Session.set('query', {});

    Session.set('query', {name: {$regex: name, $options: 'i'}});
  },
  'click .chapterAction': function (e,t) {
    Meteor.call('selectChapter', t.data._id, e.currentTarget.dataset.id);
  },
});

Template.ChapterSelect.helpers({
  hasChapters: function () {
    return (Chapters.find({}).count() > 0);
  },
  chapters: function () {
    var chapters = Chapters.find(Session.get('query'),{sort: {voteCount: -1}}).fetch();

    var rejected = this.rejected;
    chapters.forEach(function (value) {
      if (_.indexOf(rejected, value._id) !== -1) value.color = {background: "#fcf8e3", border: "#8a6d3b"};
    });

    return chapters;
  },
});

Template.ChapterSelect.created = function () {
  Session.set('query', {});
}

Template.ChapterSelect.destroyed = function () {
  delete Session.keys['query'];
}