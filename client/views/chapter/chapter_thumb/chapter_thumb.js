Template.ChapterThumb.helpers({
  getVotes: function () {
    var aux = ChaptersStats.findOne({chapterId: this.data._id}, {fields: {voteCount: 1}});
    if (aux) return aux.voteCount;
  },
  forks: function() {
    // Every branch count
    var aux = 0;
    this.data.childChapters.choices.forEach(function(value) {
      aux += value.chaptersCount;
    });

    return aux;
  },
  getColor: function () {
    if (this.color) return 'background-color: ' + this.color.background + 
                           '; border-color: ' + this.color.border + ';';
  },
});