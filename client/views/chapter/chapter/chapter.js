var commentButtonsShow = function () {
  $('#new-comment-container').addClass('on');
  $('#new-comment-buttons').show();
  $('#new-comment-input').attr('rows', 4);
}

var commentButtonHide = function () {
  $('#new-comment-container').removeClass('on');
  $('#new-comment-buttons').hide();
  $('#new-comment-input').attr('rows', 2);
}

Template.Chapter.events({
  'click #comment-anonymous-label': function (e,t) {
    $('#comment-anonymous').prop('checked', !$('#comment-anonymous').is(':checked'));
  },
  'click #comment-submit': function (e,t) {
    var text = $('#new-comment-input').val();
    if (text == '') return;

    var data = {
      text: text,
      chapterId: t.data.data._id,
    }

    Comments.insert(data, function (e,r) {
      commentButtonHide();
      $('#new-comment-input').val('');

      if (e) return console.log(e);
    });
  },
  'focus .comment-inputs': function (e,t) {
    commentButtonsShow();
  },
  'blur .comment-inputs': function (e,t) {
    commentButtonHide();
  },
  'click #plus-choice': function (e,t) {
    Session.set('newChoice', !Session.get('newChoice'));
    Meteor.setTimeout(function(){$('#new-choice-input').focus()}, 200);
    return false;
  }, 
  'click #show-choices': function (e,t) {
    Session.set('showChoices', !Session.get('showChoices'));
    Session.set('newChoice', false);
  },
  'submit #new-choice': function(e,t) {
    e.preventDefault();

    var name = $('#new-choice-input').val();

    if (name == '') return;

    Meteor.call('newChoice', toTitleCase(name), t.data.data._id);
    Session.set('newChoice', false);
  },
  'click #random-choice': function (e,t) {
    var id = Session.get('run');
    var run = Runs.findOne({_id: id});

    if (!run) return;


    var choice = Math.floor((Math.random() * t.data.data.childChapters.choices.length));


    var history = run.history;
    history[history.length - 1].choice = choice;

    var newChapter = randomChapter(run, t.data.data.childChapters.choices[choice].chapters);

    if (newChapter) {
      history.push({chapter: newChapter});
    }

    Session.set('choice', choice);

    Meteor.setTimeout(
      function() {
        Runs.update({_id: id}, {$set: {history: history, rejected: []}});
        Session.set('choice', null);
      },
      2000
    );
  },
  'click #downvote': function (e,t) {
    Meteor.call('downVote', t.data.data._id);
  },
  'click #upvote': function (e,t) {
    Meteor.call('upVote', t.data.data._id);
  },
  'click #tags': function (e,t) {
    $('.modalDialog').addClass('on');
    $('#new-tag-name').focus();
  },
  'click .play-sound': function (e,t) {
    var audio = document.getElementById('sound-' + e.currentTarget.id);
    audio.play();
  },
  'click #report-nsfw': function (e,t) {
    Meteor.call('toggleReportChapter', t.data.data._id, "nsfw");
  },
  'click #report-spam': function (e,t) {
    Meteor.call('toggleReportChapter', t.data.data._id, "spam");
  },
  'click #report-copyright': function (e,t) {
    Meteor.call('toggleReportChapter', t.data.data._id, "copyright");
  },
  'click #hot-order': function () {
    var mod = Session.get('modifiers');
    mod.sort = {voteCount: -1};
    Session.set('modifiers', mod);
  },
  'click #fresh-order': function () {
    var mod = Session.get('modifiers');
    mod.sort = {createdAt: -1};
    Session.set('modifiers', mod);
  },
  'click .load-more-comments': function (e,t) {
    var query = {
      chapterId: t.data.data._id,
      parentId: {$exists: false},
    }

    var mod = Session.get('modifiers');
    mod.limit = Number(e.currentTarget.getAttribute('comments-loaded'));
    if (mod.limit == NaN) return;

    mod.limit += 5;
    e.currentTarget.setAttribute('comments-loaded', mod.limit);
    
    showLoading('');
    Meteor.subscribe('comments', query, mod, function (e,r) {
      if (e) console.log(e);

      hideLoading('', query, mod.limit);
    });
  },
});

Template.Chapter.helpers({
  getCommentsOrder: function (str) {
    var mod = Session.get('modifiers');
    if ((mod.sort.voteCount && str == 'hot') ||
        (mod.sort.createdAt && str == 'fresh'))
      return 'on';
  },
  getCommentsCount: function () {
    return Session.get('commentsCount');
  },
  expanding: function () {
    if (this.data && this.data.childChapters && this.data.childChapters.type == "expanding") {
      return true;
    }

    return false;
  },
  newChoice: function () {
    return Session.get('newChoice');
  },
  random: function () {
    if (this.data && this.data.childChapters && this.data.childChapters.type == "random") {
      return 'disabled';
    }

    return;
  },
  upVoted: function () {
    if (!this.data || !this.data.stats || !this.data.stats.upVotes) return;

    if (_.indexOf(this.data.stats.upVotes, Meteor.userId()) != -1) {
      return "green-container";
    }

    return '';
  },
  downVoted: function () {
    if (!this.data || !this.data.stats || !this.data.stats.downVotes) return;

    if (_.indexOf(this.data.stats.downVotes, Meteor.userId()) != -1) {
      return "red-container";
    }

    return '';
  },
  sortedTags: function () {
    if (!this.data || !this.data.stats || !this.data.stats.tags) return;

    var aux = _.sortBy(this.data.stats.tags, function(tag) {return tag.count});
    return aux.reverse().slice(0, 8);
  },
  sortedChoices: function () {
    if (!this.data || !this.data.childChapters || !this.data.childChapters.choices) return;

    var random;
    if (this.data.childChapters.type == "random") {
      random = false;
    } else {
      random = true;
    }

    var aux = this.data.childChapters.choices;
    aux.forEach(function(value, index) {
      value.index = index;
      value.random = random;
    });

    aux = _.sortBy(aux, function(choice) {return choice.count});
    return aux.reverse();
  },
  reported: function (type) {
    if (!this.data || !this.data.stats || !this.data.stats.reports) return;

    if (_.indexOf(this.data.stats.reports[type].users, Meteor.userId()) != -1) {
      return 'reportedChapter';
    }
  },
  showChoices: function () {
    return Session.get('showChoices');
  },
  commentsAutorun: function () {
    if (!this.data) return;

    Meteor.call('commentCount', {chapterId: this.data._id}, function (e,r) {
      if (e) return console.log(e);

      Session.set('commentsCount', r);
    });

    var query = {
      chapterId: this.data._id,
      parentId: {$exists: false},
    }

    var mod = Session.get('modifiers');
    mod.limit = 5;

    showLoading('');
    Meteor.subscribe('comments', query, mod, function (e,r) {
      if (e) console.log(e);

      hideLoading('', query, 5);
    });
  },
  comments: function (chapterId, parentId) {
    var query = {
      chapterId: chapterId,
      parentId: {$exists: false},
    }

    return Comments.find(query, Session.get('modifiers'));
  },
  getPostType: function () {
    if (Session.get('curReply') == '') return 'Post';

    return 'Reply';
  }
});

Template.NewReply.events({
  'click #reply-cancel': function (e,t) {
    Session.set('curReply', '');
  },
  'click #reply-submit': function (e,t) {
    var text = $('#new-reply-input').val();
    if (text == '') return;

    var data = {
      text: text,
      chapterId: t.data.data.chapterId,
      parentId: Session.get('curReply'),
    }

    Comments.insert(data, function (e,r) {
      commentButtonHide();
      Session.set('curReply', '');
      $('#new-comment-input').val('');

      if (e) return console.log(e);
    });
  },
});

Template.Comment.events({
  'click .comment-reply': function (e,t) {
    Session.set('curReply', e.currentTarget.id.substring(6));

    Meteor.setTimeout(function() {$('#new-reply-input').focus();}, 100);
  },
  'click .upvoteComment': function (e,t) {
    Meteor.call('upVoteComment', e.currentTarget.id.substring(3));
  },
  'click .downvoteComment': function (e,t) {
    Meteor.call('downVoteComment', e.currentTarget.id.substring(5));
  },
  'click .load-more-comments': function (e,t) {
    var parentId = t.data.data._id;
    var query = {
      chapterId: t.data.data.chapterId,
      parentId: parentId,
    }

    var mod = Session.get('modifiers');
    mod.limit = Number(e.currentTarget.getAttribute('comments-loaded'));
    if (mod.limit == NaN) return;

    mod.limit += 5;
    e.currentTarget.setAttribute('comments-loaded', mod.limit);
    
    showLoading(parentId);
    Meteor.subscribe('comments', query, mod, function (e,r) {
      if (e) console.log(e);

      hideLoading(parentId, query, mod.limit);
    });

    return false;
  },
});

Template.Comment.helpers({
  isReplying: function (id) {
    if (Session.get('curReply') == id) return true;
  },
  getTime: function (date) {
    var duration = moment.duration((new Date())-date);

    return duration.humanize();
  },
  comments: function (chapterId, parentId) {
    if (!parentId || parentId == '') return;

    var query = {
      chapterId: chapterId,
      parentId: parentId,
    }

    return Comments.find(query, Session.get('modifiers'));
  },
  commentAutoRun: function (chapterId, parentId) {
    var query = {
      chapterId: chapterId,
      parentId: parentId,
    }

    var mod = Session.get('modifiers');
    mod.limit = 2;

    var parentSave = parentId;

    showLoading(parentSave);
    Meteor.subscribe('comments', query, mod, function (e,r) {
      if (e) console.log(e);

      hideLoading(parentSave, query, 2);
    });
  }
});

var showLoading = function (id) {
  $('#load-'+id).hide();
}

var hideLoading = function (id, query, count) {
  var callback = function (e,r) {
    if (e) return console.log(e);

    if (r > count) {
      $('#load-'+id).show();
    }
  }

  Meteor.call('commentCount', query, callback);
}

Template.Choice.events({
  'click .choice-select': function (e,t) {
    var id = Session.get('run');
    var run = Runs.findOne({_id: id});

    if (!run || !run.history || run.history.length < 1) return;

    var history = run.history;
    var chapterId = history[history.length - 1].chapter;

    var data = {rejected: []}
    var str = "history." + (history.length-1) + ".choice";
    data[str] = t.data.data.index;

    Runs.update({_id: id}, {$set: data});
    Meteor.call('countChoice', t.data.data.index, chapterId);
  },
  'mouseenter .thumb-topExperience': function () {
    $('#experience-' + this._id).addClass("on");
  },
  'mouseleave .thumb-topExperience': function () {
    $('#experience-' + this._id).removeClass("on");
  }
});


Template.Choice.helpers({
  randomed: function () {
    var aux = Session.get('choice');
    if (aux == this.data.index) {
      return 'randomed';
    }
  },
});


Template.Chapter.created = function () {
  Session.set('choice', null);
  Session.set('newChoice', false);
  Session.set('showChoices', false);
  Session.set('commentsCount', 0);
  Session.set('curReply', '');
  Session.set('modifiers', {sort: {voteCount: -1}});
};

Template.Chapter.rendered = function () {
  var self = this;
  
  this.autorun( function () {
    var comments = Comments.find({}).fetch();
    var userIds = _.uniq(_.pluck(comments, "userId"));

    self.subscribe("usersList", userIds);
  });
}

Template.Chapter.destroyed = function () {
  delete Session.keys['choice'];
  delete Session.keys['newChoice'];
  delete Session.keys['showChoices'];
  delete Session.keys['commentsCount'];
  delete Session.keys['curReply'];
  delete Session.keys['modifiers'];
}