Template.BrowseSide.events({
  'click .fa-toggle-off': function(e,t) {
    e.currentTarget.className = "fa filter-toggle fa-toggle-off toggle-off";
    var newQuery = query.get();

    if (e.currentTarget.id == 'view-filter')
      delete newQuery["viewPermissions.type"];
    else
      delete newQuery["writePermissions.type"];

    query.set(newQuery);
  },
  'click .toggle-off': function(e,t) {
    e.currentTarget.className = "fa filter-toggle fa-toggle-on";
    var newQuery = query.get();

    if (e.currentTarget.id == 'view-filter')
      newQuery["viewPermissions.type"] = 'public';
    else
      newQuery["writePermissions.type"] = 'public';

    query.set(newQuery);
  },
  'click .fa-toggle-on': function(e,t) {
    e.currentTarget.className = "fa filter-toggle fa-toggle-off"
    var newQuery = query.get();

    if (e.currentTarget.id == 'view-filter')
      newQuery["viewPermissions.type"] = 'private';
    else
      newQuery["writePermissions.type"] = 'private';

    query.set(newQuery);
  },
  'submit #name-filter': function(e,t) {
    e.preventDefault();

    var name = t.$('#name-input-filter').val();
    var newQuery = query.get();

    if (name == '')
      delete newQuery.name;
    else
      newQuery.name = {$regex: name, $options: 'i'};

    query.set(newQuery);
  },
  'keydown #lvl-filter': function(e,t) {
    if (e.keyCode != 13) return;

    var newQuery = query.get();

    var ini = parseInt($('#lvl-ini').val());
    if (!ini || ini == NaN || ini < 0) ini = 0;

    var end = $('#lvl-end').val();
    if (end === '') {
      newQuery.minLvl = {
        $gte: ini,
      };
      return query.set(newQuery);
    }

    end = parseInt(end);
    if (!end || end == NaN || end <= 0) end = 1;

    if (ini > end) {
      var aux = ini;
      ini = end;
      end = aux;
    }

    $('#lvl-end').val(end);
    $('#lvl-ini').val(ini);

    newQuery.minLvl = {
      $gte: ini,
      $lt: end,
    };
    query.set(newQuery);
  },
  'keydown #tag-filter': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("tagFilter");

    var input = $('#tag-filter').val();
    if (input == '') return false;

    input = toTitleCase(input);

    for (var i = 0; i < aux.length; i++) {
      if (aux[i].value == input) {
        return false;
      }
    }

    aux.push({
      value: input,
      key: input,
      name: "tagFilter"
    });
    Session.set("tagFilter", aux);

    $('#tag-filter').val('');

    return false;
  },
});

Template.BrowseSide.helpers({
  reactiveTags: function() {
    return Session.get('allTags');
  }
});

var query;
var modifiers;

Template.BrowseSide.created = function () {
  Session.set("tagFilter", []);

  query = new ReactiveVar({});
  modifiers = new ReactiveVar({limit: 9, sort: {"views.count": -1}});

  var self = this;
  this.autorun( function () {
    var curQuery = query.get();
    if (Session.get('tagFilter').length > 0)
      curQuery.tags = {$in: _.pluck(Session.get('tagFilter'), 'value')};
    else
      delete curQuery.tags;

    var mod = modifiers.get();

    $('#load-more-worlds').hide();
    Meteor.call("worldCount", curQuery, function (e,r) {
      if (e) return console.log(e);

      if (r > mod.limit)
        $('#load-more-worlds').show();
    });

    self.subscribe("queryWorlds", curQuery, mod);
  });

  Session.set('allTags', []);
  Meteor.call('getAllTags', function (e, tags) {
    if (e) return console.log(e);

    Session.set('allTags', tags);
    Meteor.setTimeout(function() {Meteor.typeahead.inject();}, 100);
  });
};

Template.BrowseSide.destroyed = function () {
  delete Session.keys['allTags'];
  delete Session.keys['tagFilter'];
};

var current = 'views';

Template.Browse.events({
  'click #date-sort': function (e,t) {
    var mod = modifiers.get();
    resetSortColors();

    if (current == 'date') {
      current = 'date2';
      $('#date-sort-fa').addClass('red-container');
      mod.sort = {createdAt: 1};
    } else {
      current = 'date';
      $('#date-sort-fa').addClass('green-container');
      mod.sort = {createdAt: -1};
    }

    modifiers.set(mod);
  },
  'click #views-sort': function (e,t) {
    var mod = modifiers.get();
    resetSortColors();

    if (current == 'views') {
      current = 'views2';
      $('#views-sort-fa').addClass('red-container');
      mod.sort = {"views.count": 1};
    } else {
      current = 'views';
      $('#views-sort-fa').addClass('green-container');
      mod.sort = {"views.count": -1};
    }

    modifiers.set(mod);
  },
  'click #chapters-sort': function (e,t) {
    var mod = modifiers.get();
    resetSortColors();

    if (current == 'chapters') {
      current = 'chapters2';
      $('#chapters-sort-fa').addClass('red-container');
      mod.sort = {chaptersCount: 1};
    } else {
      current = 'chapters';
      $('#chapters-sort-fa').addClass('green-container');
      mod.sort = {chaptersCount: -1};
    }

    modifiers.set(mod);
  },
  'click #lvl-sort': function (e,t) {
    var mod = modifiers.get();
    resetSortColors();

    if (current == 'lvl') {
      current = 'lvl2';
      $('#lvl-sort-fa').addClass('red-container');
      mod.sort = {minLvl: 1};
    } else {
      current = 'lvl';
      $('#lvl-sort-fa').addClass('green-container');
      mod.sort = {minLvl: -1};
    }

    modifiers.set(mod);
  },
  'click #load-more-worlds': function (e,t) {
    var mod = modifiers.get();
    mod.limit += 9;
    modifiers.set(mod);
  },
});

resetSortColors = function() {
  $('#date-sort-fa').removeClass('green-container red-container');
  $('#views-sort-fa').removeClass('green-container red-container');
  $('#chapters-sort-fa').removeClass('green-container red-container');
  $('#lvl-sort-fa').removeClass('green-container red-container');
}

Template.Browse.helpers({
  filteredWorlds: function () {
    var curQuery = query.get();

    if (Session.get('tagFilter').length > 0)
      curQuery.tags = {$in: _.pluck(Session.get('tagFilter'), 'value')};
    else
      delete curQuery.tags;

    return Worlds.find(curQuery, modifiers.get());
  }
});


Template.WorldThumb.helpers({
  viewPermission: function () {
    if (!this.data || !this.data.viewPermissions) return;

    if (this.data.viewPermissions.type == 'private') {
      if (_.indexOf(this.data.viewPermissions.allowed, Meteor.userId()) != -1) {
        return 'permission-granted';
      }

      return 'permission-private';
    }

    return 'permission-public';
  },
  viewPermissionMsg: function () {
    if (!this.data || !this.data.viewPermissions) return;

    if (this.data.viewPermissions.type == 'private') {
      if (_.indexOf(this.data.viewPermissions.allowed, Meteor.userId()) != -1) {
        return 'You are allowed to view chapters from this World';
      }

      return 'Private view of this World';
    }

    return 'Public view of this World';
  },
  writePermission: function () {
    if (!this.data || !this.data.writePermissions) return;
    
    if (this.data.writePermissions.type == 'private') {
      if (_.indexOf(this.data.writePermissions.allowed, Meteor.userId()) != -1) {
        return 'permission-granted';
      }
      
      return 'permission-private';
    }

    return 'permission-public';
  },
  writePermissionMsg: function () {
    if (!this.data || !this.data.writePermissions) return;
    
    if (this.data.writePermissions.type == 'private') {
      if (_.indexOf(this.data.writePermissions.allowed, Meteor.userId()) != -1) {
        return 'You are allowed to write chapters for this World';
      }
      
      return 'Private writting on this World';
    }

    return 'Public writting on this World';
  }
});

Template.WorldThumb.events({
  'click #play-world': function(e,t) {
    Meteor.call('newRun', t.data.data.startingChapter, function(error, result) {
      if (error) {
        $("#error-run").text(error.reason);
        Meteor.setTimeout(function() {$("#error-run").text('');}, 3000);
        return;
      }

      Router.go("run", {id: result});
    });
  }
});