Template.RunSide.events({
  'click #undo-choice': function (e,t) {
    var num = t.data.history.length -1;
    if (t.data.history[num].choice != undefined) {
      delete(t.data.history[num].choice);
    } else {
      t.data.history.pop();
      delete(t.data.history[num-1].choice);
    }

    Runs.update({_id: t.data._id}, {$set: {history: t.data.history}});
  },
  'click #delete-run': function(e,t) {
    if (Session.get('deleteRun')) {
      Runs.remove({_id: t.data._id}, function() {
        Router.go('home');
      });
      return;
    }

    Session.set('deleteRun', true);
    Meteor.setTimeout(function() {Session.set('deleteRun', false)}, 5000);
  },
  'click #reject-chapter': function (e,t) {
    var aux = t.data.history.pop();

    Runs.update(
      {_id: t.data._id},
      {
        $push: {rejected: aux.chapter},
        $pull: {history: aux}
      }
    );
  },
  'click #share-run': function (e,t) {
    $('#share-run').hide();
    $('#share-url').show();
    $('#share-url-input').focus().select();
  },
});

Template.RunSide.helpers({
  notFirst: function () {
    if (this.history &&
        (this.history[this.history.length -1].choice != undefined ||
        this.history.length -1 != 0)) {
      return true;
    }

    return false;
  },
  isRun: function () {
    if (Router.current().options.route._path == '/run/:id') {
      return 'current-side';
    }
  },
  isNewChapter: function () {
    if (Router.current().options.route._path == '/chapter/new') {
      return 'current-side';
    }
  },
  isHistory: function () {
    if (Router.current().options.route._path == '/run/history/:id') {
      return 'current-side';
    }
  },
  isSettings: function () {
    if (Router.current().options.route._path == '/run/options/:id') {
      return 'current-side';
    }
  },
  showUndo: function () {
    var path = Router.current().options.route._path;
    if (path == '/run/history/:id' || path == '/run/:id') {
      return true;
    }
  },
  showRefresh: function () {
    var num = this.history.length -1;
    if (this.history[num].choice == undefined) {
      return true;
    }
  },
  showNewChapter: function () {
    var world = Worlds.findOne({_id: this.worldId});

    if (!world) return;

    if (world.writePermissions.type == 'public' ||
        _.indexOf(world.writePermissions.allowed, Meteor.userId()) != -1) {
      return true;
    }
  },
  deleteText: function() {
    if (Session.get('deleteRun')) {
      return '<b>Are you sure?</b>';
    }

    return 'Delete Run';
  },
  getShareUrl: function () {
    return 'crowdventure.meteor.com/run/share/' + Session.get('run');
  },
});

Template.Run.helpers({
  userSelect: function () {
    return (this.chapterSelection === 'Select');
  },
  currentChapter: function() {
    if (this.history) {
      var id = this.history[this.history.length -1].chapter;
      var aux = Chapters.findOne({_id: id});
      if (!aux) return;

      aux.stats = ChaptersStats.findOne({chapterId: id});
      return aux;
    }
  },
  hasChapter: function () {
    if (this.history && this.history[this.history.length -1].choice == undefined) {
      return true;
    }

    return false;
  },
  checkForChapter: function () {
    if (!this.history) return;

    var num = this.history[this.history.length -1].choice;
    if (num == undefined) return;

    Chapters.find();

    Meteor.call('randomChapter', this._id);
  },
});

/*****************************************************************************/
/* Run: Lifecycle Hooks */
/*****************************************************************************/
Template.RunSide.created = function () {
  Session.set('deleteRun', false);
};

Template.Run.rendered = function () {
  var self = this;

  this.autorun( function () {
    var run = Runs.findOne(self.data._id);
    // var run = self.data;
    if (!run || !run.history || run.history.length < 1) return;

    var choice = run.history[run.history.length -1].choice;
    var id = run.history[run.history.length -1].chapter;

    self.subscribe("lastRunChapters", id, choice);
    self.subscribe("lastRunChaptersStats", id, choice);
  });
}

Template.NoChapter.events({
  'click #refresh-rejected': function (e,t) {
    Runs.update({_id: t.data._id}, {$set: {rejected: []}});
  },
});

Template.NoChapter.helpers({
  showNewChapter: function () {
    var world = Worlds.findOne({_id: this.worldId});

    if (!world) return;

    if (world.writePermissions.type == 'public' ||
        _.indexOf(world.writePermissions.allowed, Meteor.userId()) != -1) {
      return true;
    }
  },
  hasRejected: function () {
    if (this.rejected && this.rejected.length > 0) {
      return true;
    }
  },
});