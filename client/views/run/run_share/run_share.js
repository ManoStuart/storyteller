Template.RunShare.created = function () {
	Meteor.call('copyRun', Session.get('run'), "import", false, function(error, result) {
    if (error) {
    	OpenInfoModal(error.toString(), 'alert alert-danger');
      Router.go("home");
      return;
    }

    Router.go("run", {id: result});
  });
};