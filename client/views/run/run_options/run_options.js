
/*****************************************************************************/
/* RunOptions: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.RunOptions.events({
  'change #run-selection': function (e,t) {
    Session.set('curSelectionType', e.currentTarget.value);
  },
  'click .fa-toggle-off': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-on";
  },
  'click .fa-toggle-on': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-off";
  },
  'submit #run-options': function(e,t) {
    e.preventDefault();

    var data = {
      name: $('#run-name').val(),
      tags: $('#run-tags').val(),
      languages: $('#run-languages').val(),
      nsfw: ($('#run-nsfw').attr('class') == 'fa fa-2x fa-toggle-off') ? false : true,
      public: ($('#run-public').attr('class') == 'fa fa-2x fa-toggle-off') ? false : true,
      chapterSelection: $('#run-selection').val(),
    }

    if (!data.tags) data.tags = [];
    if (!data.languages) data.languages = [];

    Runs.update({_id: t.data._id}, {$set: data}, function (error) {
      if (error) console.log(error);
      Router.go('run', {id: t.data._id});
    });
  }
});

Template.RunOptions.helpers({
  setUp: function () {
    if (this.chapterSelection) {
      Session.set('curSelectionType', this.chapterSelection);
    }
  },
  availableTags: function () {
    var aux = Worlds.findOne({_id: this.worldId});
    if (aux) {
      Meteor.setTimeout(function(){$('.selectpicker').selectpicker('refresh');}, 200);
      tags = this.tags;
      return aux.tags.map(function(tag) {
        if (_.indexOf(tags, tag.name) != -1) {
          tag.selected = 'selected';
        }

        return tag;
      });
    }
  },
  getNSFW: function () {
    if (this.nsfw) {
      return 'fa-toggle-on';
    }

    return 'fa-toggle-off';
  },
  getPublic: function () {
    if (this.public) return 'fa-toggle-on';

    return 'fa-toggle-off';
  },
  languageSelected: function(lang) {
    if (_.indexOf(this.languages, lang) != -1) {
      return 'selected';
    }
  },
  selectionSelected: function (type) {
    if (this.chapterSelection === type) return 'selected';
  },
  selectionMsg: function () {
    if (Session.get('curSelectionType') === 'Top')
      return 'Top rated chapter automatically selected after each choice.';
    else if (Session.get('curSelectionType') === 'Random')
      return 'Random chapter automatically selected after each choice.';
    else if (Session.get('curSelectionType') === 'Select')
      return 'User selects the next chapter after each choice.';
  },
});

/*****************************************************************************/
/* RunOptions: Lifecycle Hooks */
/*****************************************************************************/
Template.RunOptions.created = function () {
};

Template.RunOptions.rendered = function () {
  $('.selectpicker').selectpicker('render');
  validate.runOptionsForm();
};

Template.RunOptions.destroyed = function () {
};


