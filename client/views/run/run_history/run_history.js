
/*****************************************************************************/
/* RunHistory: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.RunHistory.events({
  'click .split-run': function (e,t) {
    var hist = t.data.history;
    for (var i = 0; i < hist.length; i++) {
      if (hist[i].chapter == e.currentTarget.id) {
        Meteor.call('copyRun', Session.get('run'), "fork", i, function(error, result) {
          if (error) {
            Router.go("home");
            return;
          }

          Router.go("run", {id: result});
        });

        return;
      }
    }
  }
});

Template.RunHistory.helpers({
  chapterInfo: function () {
    return Chapters.findOne({_id: this.chapter});
  }
});


Template.ChapterHist.events({
  'click .expand': function(e,t) {
    var aux = Session.get('expand');

    aux.push(t.data.data._id);
    Session.set('expand', aux);
  },
  'click .shrink': function(e,t) {
    var aux = Session.get('expand');
    var index = _.indexOf(aux, t.data.data._id);

    aux.splice(index, 1);
    Session.set('expand', aux);
  },
  'click .play-sound': function (e,t) {
    var audio = document.getElementById('sound-' + e.currentTarget.id);
    audio.play();
  }
});

Template.ChapterHist.helpers({
  hasChoice: function () {
    if (this.hist.choice != undefined) {
      return true;
    }
    return false;
  },
  choice: function () {
    return this.data.childChapters.choices[this.hist.choice].name;
  },
  expand: function () {
    if (_.indexOf(Session.get('expand'),this.data._id) != -1) return "expanded";

    return "contracted";
  },
  isExpanded: function () {
    if (_.indexOf(Session.get('expand'),this.data._id) != -1) return true;

    return false;
  }
});

/*****************************************************************************/
/* RunHistory: Lifecycle Hooks */
/*****************************************************************************/
Template.RunHistory.created = function () {
  Session.set('expand', []);
};

Template.RunHistory.rendered = function () {
};

Template.RunHistory.destroyed = function () {
};


