Template.ArrayInput.helpers({
  array: function () {
    return Session.get(this.name);
  },
});

Template.ArrayIcon.events({
  'click .remove-array': function (e,t) {
    var aux = Session.get(t.data.data.name);

    for (var i = 0; i < aux.length; i++) {
      if (aux[i].value === e.currentTarget.id) {
        break;
      }
    }

    aux.splice(i,1);
    Session.set(t.data.data.name, aux);
  }
});


Template.ArrayIcon.helpers({
  getColor: function () {
    if (!this.data || !this.data.color) return;

    return "background-color: " + this.data.color + ";";
  },
});