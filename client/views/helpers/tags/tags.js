
/*****************************************************************************/
/* Tags: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Tags.events({
  'click .report-tag': function (e,t) {
    Meteor.call('toggleReportTag', e.currentTarget.id, t.data.id);
    return false;
  },
  'click .add-tag': function(e,t) {
    Meteor.call('addTag', e.currentTarget.id, t.data.id);
  },
  'click .remove-tag': function(e,t) {
    Meteor.call('removeTag', e.currentTarget.id, t.data.id);
  },
  'submit #new-tag': function(e,t) {
    e.preventDefault();

    var name = $('#new-tag-name').val();

    if (name === '') return;

    name = toTitleCase(name);

    Meteor.call('addTag', name, t.data.id);
    $('#new-tag-name').val('');
  },
  'click #close-tag': function (e,t) {
    $('.modalDialog').removeClass('on');
  },
  'keydown' : function (e,t) {
    if (e.keyCode == 27) {
      $('.modalDialog').removeClass('on');
    }
  }
});

Template.Tags.helpers({
  myTags: function() {
    if (!this.tags) return;

    var aux = [];

    for(var i = 0; i < this.tags.length; i++) {
      if (_.indexOf(this.tags[i].users, Meteor.userId()) != -1) {
        aux.push(this.tags[i]);
      }
    }

    return aux;
  },
  sortedTags: function () {
    if (!this.tags) return;
    
    var aux = _.sortBy(this.tags, function(tag) {return tag.count});
    return aux.reverse().slice(0, 20);
  },
  reported: function () {
    if (this.reports && _.indexOf(this.reports.users, Meteor.userId()) != -1) {
      return "reported";
    }
  },
  reportTooltip: function () {
    if (this.reports && _.indexOf(this.reports.users, Meteor.userId()) != -1) {
      return "You have reported this tag as being incorrectly applied.";
    }

    return "Report this tag as being incorrectly applied.";
  }
});