OpenInfoModal = function (msg, className) {
  Session.set('infoMessage', msg);

  if (!className) className == '';
  $('.infoModal > .modal-content')[0].className = 'modal-content ' + className;

  $('#infoModal').modal('show');
}

Template.InfoModal.helpers({
  getInfoMsg: function () {
    return Session.get('infoMessage');
  }
});

Template.InfoModal.created = function () {
  Session.set('infoMessage', '');
}

Template.InfoModal.destroyed = function () {
  delete Session.keys['infoMessage'];
}