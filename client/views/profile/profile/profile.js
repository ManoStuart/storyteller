Template.Profile.helpers({
  writeCount: function () {
    return Chapters.find({userId: this._id}).count();
  },
  commentCount: function () {
    return 0;
  },
  runs: function () {
    return Runs.find({userId: this._id}, {sort: {updatedAt: -1}})
  },
  chapters: function () {
    return Chapters.find({userId: this._id}, {sort: {createdAt: -1}});
  },
  chapter: function () {
    var id = this.history[this.history.length -1].chapter;
    return Chapters.findOne({_id: id});
  },
  worlds: function () {
    return Worlds.find({userId: Meteor.userId()}, {sort: {createdAt: -1}});
  },
  hasWorlds: function () {
    if (Worlds.find({userId: Meteor.userId()}).count() > 0) {
      return true;
    }

    return false;
  },
  hasChapters: function () {
    if (Chapters.find({userId: Meteor.userId()}).count() > 0) {
      return true;
    }

    return false;
  },
  hasRuns: function () {
    if (Runs.find({userId: Meteor.userId()}).count() > 0) {
      return true;
    }

    return false;
  },
  curExp: function () {
    if (!this.profile) return '0px';

    var aux = 100 - this.profile.exp;
    return aux+'%';
  },
  showRuns: function () {
    return showRuns.get();
  },
  showChapters: function () {
    return showChapters.get();
  },
  showWorlds: function () {
    return showWorlds.get();
  }
});

Template.Profile.events({
  'click #show-runs': function (e,t) {
    showRuns.set(!showRuns.get());
  },
  'click #show-chapters': function (e,t) {
    showChapters.set(!showChapters.get());
  },
  'click #show-worlds': function (e,t) {
    showWorlds.set(!showWorlds.get());
  },
  'click .chapterAction': function (e,t) {
    Router.go('editChapter', {id: e.currentTarget.dataset.id});
  },
});

var showRuns;
var showChapters;
var showWorlds;

Template.Profile.created = function () {
  Session.set("tagFilter", []);

  showRuns = new ReactiveVar(false);
  showChapters = new ReactiveVar(false);
  showWorlds = new ReactiveVar(false);
}

Template.RunThumb.helpers({
  lastChoice: function () {
    if (!this.data || !this.run) return;

    var num = this.run.history[this.run.history.length -1].choice;
    if (num != undefined) {
      return this.data.childChapters.choices[num].name;
    }
  },
  runTooltip: function() {
    if (!this.data || !this.run) return;

    var num = this.run.history[this.run.history.length -1].choice;
    if (num != undefined) {
      return 'your choice';
    }

    return 'current chapter';
  },
  forks: function() {
    if (this.run) {
      var choice = this.run.history[this.run.history.length-1].choice;

      // Single branch count
      if (choice != undefined) {
        return this.data.childChapters.choices[choice].chaptersCount;
      }
    }

    // Every branch count
    var aux = 0;
    this.data.childChapters.choices.forEach(function(value) {
      aux += value.chaptersCount;
    });

    return aux;
  },
});