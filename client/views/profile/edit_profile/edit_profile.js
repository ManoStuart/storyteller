
/*****************************************************************************/
/* EditProfile: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.EditProfile.events({
  'click .fa-toggle-off': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-on";
  },
  'click .fa-toggle-on': function(e,t) {
    e.currentTarget.className = "fa fa-2x fa-toggle-off";
  },
  'submit #EditProfile': function(e,t) {

  }
});

Template.EditProfile.helpers({
  toggle: function () {
    var user = Meteor.user()
    if (user) {
      if (user.profile.public) {
        $('#profile-public').addClass('fa-toggle-on');
      } else {
        $('#profile-public').addClass('fa-toggle-off');
      }
    }
  }
});

/*****************************************************************************/
/* EditProfile: Lifecycle Hooks */
/*****************************************************************************/
Template.EditProfile.created = function () {
};

Template.EditProfile.rendered = function () {
};

Template.EditProfile.destroyed = function () {
};


