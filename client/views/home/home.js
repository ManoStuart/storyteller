
/*****************************************************************************/
/* Home: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Home.events({
  'click #random-world': function (e,t) {
    Meteor.call('newRandomRun', function (error, result) {
      if (error) return OpenInfoModal(error.toString(), 'alert alert-danger');

      Router.go("run", {id: result});
    });
  }
});

Template.Home.helpers({
  randomPhoto: function() {
    return Math.floor((Math.random() * 6) + 1);
  },
  popularWorlds: function () {
    return Worlds.find(
      {},
      {
        sort: {"views.count": -1},
        limit: 6
      }
    );
  }
});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.created = function () {
};

Template.Home.rendered = function () {
  var wd = $('.character').width();
  $('.fa-gamepad').css('font-size', wd*0.1);
};

Template.Home.destroyed = function () {
};