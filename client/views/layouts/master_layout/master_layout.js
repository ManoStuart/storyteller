
/*****************************************************************************/
/* MasterLayout: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.MasterLayout.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.MasterLayout.helpers({
  offset: function () {
    if (Session.get('sideBar')) {
      return 'col-md-offset-1';
    }
  }
});

Template.ProfileHeader.helpers({
  lastRun: function () {
    var run = Runs.findOne({userId: Meteor.userId()}, {sort: {updatedAt: -1}});
    if (run) {
      return run._id;
    }
  },
  hasRun: function () {
    return (Runs.find({userId: Meteor.userId()}).count() > 0);
  }
});

/*****************************************************************************/
/* MasterLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.MasterLayout.created = function () {
};

Template.MasterLayout.rendered = function () {
};

Template.MasterLayout.destroyed = function () {
};

Template.ProfileHeader.events({
 'click #logout': function (e,t) {
    Meteor.logout();
 }
});