
/*****************************************************************************/
/* EditWorld: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.EditWorld.events({
  'keydown #allowed-view-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("viewPermissions");

    var input = $('#allowed-view-input').val();
    if (input == '') {
      return false;
    }

    Meteor.call('getUserId', input, function (e, user) {
      if (e || !user) {
        $("#error-view").text('User not found');
        Meteor.setTimeout(function() {$("#error-view").text('');}, 3000);
        return false;
      }

      aux.push({
        value: user._id,
        key: input,
        name: "viewPermissions"
      });
      Session.set("viewPermissions", aux);

      $('#allowed-view-input').val('');
    });

    return false;
  },
  'keydown #allowed-write-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("writePermissions");
    var input = $('#allowed-write-input').val();
    if (input == '') {
      return false;
    }

    Meteor.call('getUserId', input, function (e, user) {
      if (e || !user) {
        $("#error-write").text('User not found');
        Meteor.setTimeout(function() {$("#error-write").text('');}, 3000);
        return false;
      }

      aux.push({
        value: user._id,
        key: input,
        name: "writePermissions"
      });
      Session.set("writePermissions", aux);

      $('#allowed-write-input').val('');
    });

    return false;
  },
  'change #world-write': function (e,t) {
    if ($('#world-write').val() == 'private') {
      Session.set(
        'writePermissions',
        [{
          value: Meteor.userId(),
          key: Meteor.user().username,
          name: "writePermissions"
        }]
      );

      return write.set(true);
    }

    Session.set('writePermissions', []);
    write.set(false);
  },
  'change #world-view': function (e,t) {
    if ($('#world-view').val() == 'private') {
      Session.set(
        'viewPermissions',
        [{
          value: Meteor.userId(),
          key: Meteor.user().username,
          name: "viewPermissions"
        }]
      );
      return view.set(true);
    }

    Session.set('viewPermissions', []);
    view.set(false);
  },
  'submit #new-world': function(e,t) {
    e.preventDefault();

    worldData = {
      name: $('#world-name').val(),
      minLvl: $('#world-lvl').val(),
      viewPermissions: {
        type: $('#world-view').val(),
        allowed: _.pluck(Session.get('viewPermissions'), 'value'),
      },
      writePermissions: {
        type: $('#world-write').val(),
        allowed: _.pluck(Session.get('writePermissions'), 'value'),
      }
    };

    if (worldData.minLvl > t.data.minLvl) {
      $('#world-lvl').addClass('error').focus();
      $('#error-editworld').text('You cant increase the world minimun Level.');
      Meteor.setTimeout(function() {
        $("#error-editworld").text('');
        $("#world-lvl").toggleClass('error');
      }, 5000);

      return false;
    }

    Worlds.update({_id: t.data._id}, {$set: worldData}, function () {
      Router.go('profile', {id: Meteor.userId()});
    });
  }
});

Template.EditWorld.helpers({
  startShit: function () {
    if (this.writePermissions && this.viewPermissions && Meteor.user()) {
      if (this.writePermissions.type == 'private') write.set(true);
      if (this.viewPermissions.type == 'private') view.set(true);

      Meteor.call('getPermissionArray', this.viewPermissions.allowed, function (e,r) {
        if (e) return console.log(e);

        Session.set("viewPermissions", r.map(function (user) {
          return {
            value: user._id,
            key: user.username,
            name: "viewPermissions",
          }
        }));
      });

      Meteor.call('getPermissionArray', this.writePermissions.allowed, function (e,r) {
        if (e) return console.log(e);

        Session.set("writePermissions", r.map(function (user) {
          return {
            value: user._id,
            key: user.username,
            name: "writePermissions",
          }
        }));
      });
    }
  },
  selectedWrite: function (type) {
    if (this.writePermissions && this.writePermissions.type == type) {
      return 'selected';
    }
  },
  selectedView: function (type) {
    if (this.viewPermissions && this.viewPermissions.type == type) {
      return 'selected';
    }
  },
  writePrivate: function () {
    return (write.get()) ? '' : 'hidden';
  },
  viewPrivate: function () {
    return (view.get()) ? '' : 'hidden';
  }
});

var write;
var view;

Template.EditWorld.created = function () {
  Session.set("viewPermissions", []);
  Session.set("writePermissions", []);

  write = new ReactiveVar(false);
  view = new ReactiveVar(false);
};

Template.EditWorld.rendered = function () {
  validate.newWorldForm();
  $('.selectpicker').selectpicker('refresh');
}