Template.NewWorld.events({
  'keydown #allowed-view-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("viewPermissions");

    var input = $('#allowed-view-input').val();
    if (input == '') {
      return false;
    }

    Meteor.call('getUserId', input, function (e, user) {
      if (e || !user) {
        $("#error-view").text('User not found');
        Meteor.setTimeout(function() {$("#error-view").text('');}, 3000);
        return false;
      }

      aux.push({
        value: user._id,
        key: input,
        name: "viewPermissions"
      });
      Session.set("viewPermissions", aux);

      $('#allowed-view-input').val('');
    });

    return false;
  },
  'keydown #allowed-write-input': function(e,t) {
    if (e.keyCode != 13) return;

    var aux = Session.get("writePermissions");
    var input = $('#allowed-write-input').val();
    if (input == '') {
      return false;
    }

    Meteor.call('getUserId', input, function (e, user) {
      if (e || !user) {
        $("#error-write").text('User not found');
        Meteor.setTimeout(function() {$("#error-write").text('');}, 3000);
        return false;
      }

      aux.push({
        value: user._id,
        key: input,
        name: "writePermissions"
      });
      Session.set("writePermissions", aux);

      $('#allowed-write-input').val('');
    });

    return false;
  },
  'change #world-write': function (e,t) {
    if ($('#world-write').val() == 'private') {
      Session.set(
        'writePermissions',
        [{
          value: Meteor.userId(),
          key: Meteor.user().username,
          name: "writePermissions"
        }]
      );

      return write.set(true);
    }

    Session.set('writePermissions', []);
    write.set(false);
  },
  'change #world-view': function (e,t) {
    if ($('#world-view').val() == 'private') {
      Session.set(
        'viewPermissions',
        [{
          value: Meteor.userId(),
          key: Meteor.user().username,
          name: "viewPermissions"
        }]
      );
      return view.set(true);
    }

    Session.set('viewPermissions', []);
    view.set(false);
  },
  'submit #new-world': function(e,t) {
    e.preventDefault();

    chapterData = {
      name: $('#chapter-name').val(),
      description: $('#chapter-description').val(),
      nsfw: ($('#nsfw').attr('class') == 'fa fa-2x fa-toggle-off') ? false : true,
      language: $('#chapter-language').val(),
      childChapters: {
        type: $('#choice-type').val(),
        choices: []
      }
    };

    // Content Validation
    var contentType = $('#chapter-content-type').val();

    if (contentType === 'image') {
      var image = $('#content-image');

      if (image.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = image.css('width', '100%').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'video') {
      var video = $('#content-video');

      if (video.attr('src') === '')
        chapterData.content = '';
      else
        chapterData.content = video.removeAttr('style').removeAttr('id')[0].outerHTML;
    } else if (contentType === 'text') {
      chapterData.content = $('#summernote').code();
    }

    if (chapterData.content === '') {
      $('#content-container').addClass('error');
      $('#error-newchapter').text('This field is required.');

      if (contentType === 'image')      
        $('#content-image-input').focus();
      else if (contentType === 'video')
        $('#content-video-input').focus();
      else if (contentType === 'text')
        $('.note-editable').focus();

      Meteor.setTimeout(function() {
        $("#error-newchapter").text('');
        $(".note-editor").toggleClass('error');
      }, 5000);

      return false;
    }

    var aux = Session.get('choices');
    aux.forEach(function(value) {
      chapterData.childChapters.choices.push({name: value.value, users: [Meteor.userId()]});
    });

    worldData = {
      name: $('#world-name').val(),
      minLvl: $('#world-lvl').val(),
      viewPermissions: {
        type: $('#world-view').val(),
        allowed: Session.get('viewPermissions').map(function(allowed) {return allowed.value})
      },
      writePermissions: {
        type: $('#world-write').val(),
        allowed: Session.get('writePermissions').map(function(allowed) {return allowed.value})
      }
    };

    Worlds.insert(worldData, function(error, worldId) {
      if (error)
        return console.log(error);

      chapterData.worldId = worldId;

      Chapters.insert(chapterData, function(err, chapterId) {
        if (err)
          return console.log(err);

        Worlds.update(
          {_id: worldId}, {$set: {startingChapter: chapterId}},
          function (e,r) {
            Meteor.call('newRun', chapterId, function(error, result) {
              if (error)
                return Router.go('home');

              Router.go("run", {id: result});
            });
          });
      });
    });
  }
});

Template.NewWorld.helpers({
  writePrivate: function () {
    return (write.get()) ? '' : 'hidden';
  },
  viewPrivate: function () {
    return (view.get()) ? '' : 'hidden';
  }
});

/*****************************************************************************/
/* NewWorld: Lifecycle Hooks */
/*****************************************************************************/
var write;
var view;

Template.NewWorld.created = function () {
  Session.set("viewPermissions", []);
  Session.set("writePermissions", []);

  write = new ReactiveVar(false);
  view = new ReactiveVar(false);
};

Template.NewWorld.rendered = function () {
  validate.newWorldForm();
};

Template.NewWorld.destroyed = function () {
};


